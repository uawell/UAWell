import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:UAWell/custom_widgets/center_text_common_button.dart';
import 'package:UAWell/custom_widgets/custom_app_bar.dart';
import 'package:UAWell/styles/styles.dart';

import '../../generated/locale_keys.g.dart';

class AboutAppScreen extends StatelessWidget {
  const AboutAppScreen({Key? key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(titleBar: '',),
      body: SingleChildScrollView(
        padding:  EdgeInsets.fromLTRB(30, 27, 30, 42),
        child: Column(
          children: [
            Text(
              LocaleKeys.about_app_screen_title.tr(),
              style: TitleDescriptionTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 19.0),
            Text(
              LocaleKeys.about_app_screen_description.tr(),
              style: DescriptionTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 42.0),
            CenterTextCommonButton(
              onPressed: () {
                // Возвращаемся на предыдущую страницу
                Navigator.pop(context);
              },
              buttonText: LocaleKeys.common_buttons_ok.tr(),
            ),
          ],
        ),
      ),
    );
  }
}
