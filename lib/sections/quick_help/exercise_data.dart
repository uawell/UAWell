// exercise_data.dart
final Map<String, List<String>> exerciseData = {
  'apathy_empty': [
    "breathing_1", "physical_exercise_2", "breathing_3", "breathing_5",
    "normalization_and_positive_meaning_assignment", "visualization"
],
  'anxiety_agitation': [
    "physical_exercise_1", "physical_exercise_2", "physical_exercise_3",
    "breathing_2", "breathing_4", "containment", "imagery_rehearsal",
    "attention_management", "normalization_and_positive_meaning_assignment",
    "visualization"
  ],
  'derealization_depersonalization': [
    "breathing_1", "physical_exercise_1", "breathing_2", "breathing_4",
    "attention_management", "normalization_and_positive_meaning_assignment"
  ],
  'difficulty_breathing': [
    "breathing_1", "physical_exercise_1", "physical_exercise_3", "breathing_2",
    "breathing_4", "containment"
  ],
  'tension_relaxation_impossibility': [
    "breathing_1", "physical_exercise_1", "physical_exercise_2",
    "physical_exercise_3", "breathing_2", "breathing_3",
    "containment", "imagery_rehearsal"
  ],
  'unpleasant_physical_reactions': [
    "breathing_1", "physical_exercise_1", "breathing_2", "breathing_4",
    "imagery_rehearsal"
  ],
  'panic': [
    "physical_exercise_1", "breathing_2", "breathing_4", "containment",
    "imagery_rehearsal", "attention_management",
    "normalization_and_positive_meaning_assignment"
  ],
  'immersion_painful_experience_flashback_traumatic_funnel': [
    "breathing_1", "physical_exercise_1", "breathing_2", "imagery_rehearsal",
    "attention_management", "normalization_and_positive_meaning_assignment"
  ],
  'bewilderment_inability_concentrate': [
    "breathing_1", "physical_exercise_1", "breathing_2", "attention_management",
    "visualization"
  ],
  'intense_unbearable_emotions': [
    "breathing_1", "physical_exercise_1", "breathing_2", "breathing_3",
    "breathing_4", "containment", "imagery_rehearsal",
    "normalization_and_positive_meaning_assignment", "somatic_knot"
  ],
  'weakness_helplessness': [
    "breathing_1", "physical_exercise_1", "physical_exercise_2",
    "physical_exercise_3", "breathing_2", "breathing_3", "breathing_5",
    "visualization"
  ],
  'shock_stupor_freeze': [
    "breathing_1", "physical_exercise_1", "physical_exercise_2", "breathing_2",
    "breathing_3", "attention_management", "normalization_and_positive_meaning_assignment"
  ],
  'emotional_thoughts': [
    "breathing_1", "physical_exercise_1", "breathing_2", "containment",
    "imagery_rehearsal", "normalization_and_positive_meaning_assignment",
    "somatic_knot"
  ],
};

final BreathingExercise = [
  "breathing_2",
  // "breathing_3",
  // "breathing_4",
  // "breathing_5",
];