import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/services.dart';
import 'package:UAWell/custom_widgets/common_page_padding.dart';
import 'package:UAWell/custom_widgets/custom_app_bar.dart';
import 'package:UAWell/sections/about_app/about_app_screen.dart';
import 'package:UAWell/sections/quick_help/quick_help_options_screen.dart';

import '../../custom_widgets/breathing_animation_widget.dart';
import '../../custom_widgets/center_text_common_button.dart';
import '../../generated/locale_keys.g.dart';
import '../../styles/styles.dart';
import '../contacts/contacts_screen.dart';
import '../therapy_options_screen.dart';
import 'exercise_data.dart';

class ExerciseDescriptionScreen extends StatefulWidget {
  final String symptomKey;

  ExerciseDescriptionScreen({Key? key, required this.symptomKey})
      : super(key: key);

  @override
  _ExerciseDescriptionScreenState createState() =>
      _ExerciseDescriptionScreenState();
}

class _ExerciseDescriptionScreenState extends State<ExerciseDescriptionScreen> {
  late int currentExerciseIndex;
  late List<String> exercisesForSymptom;
  late List<String> isBreathingExercise;
  bool showNextButton = true;

  @override
  void initState() {
    super.initState();
    exercisesForSymptom = exerciseData[widget.symptomKey] ?? [];
    currentExerciseIndex = 0;
    if (exercisesForSymptom.length == 1) {
      showNextButton = false;
    }
  }

  void displayNextExercise() {
    setState(() {
      currentExerciseIndex++;
      if (currentExerciseIndex == exercisesForSymptom.length - 1) {
        showNextButton = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    bool isBreathingExercise =
        BreathingExercise.contains(exercisesForSymptom[currentExerciseIndex]);

    return Scaffold(
      appBar: CustomAppBar(
        titleBar: '',
      ),
      body: CommonPagePadding(
        commonPage: Column(
          children: [
            Text(
              tr('quick_help_exercise.${exercisesForSymptom[currentExerciseIndex]}.name'),
              style: TitleDescriptionTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 17),
            Text(
              tr('quick_help_exercise.${exercisesForSymptom[currentExerciseIndex]}.description'),
              style: DescriptionTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 27),
            if (showNextButton)
              Column(
                children: [
                  if (isBreathingExercise)
                    Container(
                      width: 210,
                      height: 210,
                      child: BreathingAnimationWidget(
                        duration: 5,
                        breathFrequency: 600,
                        symptomKey: exercisesForSymptom[currentExerciseIndex],
                      ),
                    ),
                  CenterTextCommonButton(
                      onPressed: () {
                        displayNextExercise();
                      },
                      buttonText: LocaleKeys.common_buttons_next.tr()),
                  SizedBox(height: 20),
                  CenterTextCommonButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AboutAppScreen(),
                          ),
                        );
                      },
                      buttonText: LocaleKeys.common_buttons_it_helped.tr()),
                ],
              )
            else
              Column(
                children: [
                  CenterTextCommonButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/QuickHelpOptionsScreen',
                            arguments: "");
                      },
                      buttonText: LocaleKeys.common_buttons_start_over.tr()),
                  SizedBox(height: 20),
                  CenterTextCommonButton(
                      onPressed: () {
                        Navigator.pushNamed(context, '/TherapyOptionsScreen',
                            arguments: '');
                      },
                      buttonText: LocaleKeys.common_buttons_it_helped.tr()),
                  SizedBox(height: 20),
                  CenterTextCommonButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ContactsScreen(),
                          ),
                        );
                      },
                      buttonText:
                          LocaleKeys.common_buttons_contact_specialist.tr()),
                ],
              ),
          ],
        ),
      ),
    );
  }
}
