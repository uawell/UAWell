import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:UAWell/sections/quick_help/symptom_description_screen.dart';

import '../../custom_widgets/common_button.dart';
import '../../custom_widgets/common_page_padding.dart';
import '../../custom_widgets/custom_app_bar.dart';
import '../../generated/locale_keys.g.dart';

class QuickHelpOptionsScreen extends StatelessWidget {
  final List<String> buttonKeys = [
    'apathy_empty',
    'anxiety_agitation',
    'derealization_depersonalization',
    'difficulty_breathing',
    'tension_relaxation_impossibility',
    'unpleasant_physical_reactions',
    'panic',
    'immersion_painful_experience_flashback_traumatic_funnel',
    'bewilderment_inability_concentrate',
    'intense_unbearable_emotions',
    'weakness_helplessness',
    'shock_stupor_freeze',
    'emotional_thoughts',
  ];
  final String title = LocaleKeys.quick_help_return_page_title;

  QuickHelpOptionsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final String prevScreenTitle =
        ModalRoute.of(context)?.settings.arguments as String;
    return Scaffold(
      appBar: CustomAppBar(titleBar: prevScreenTitle),
      body: CommonPagePadding(
        commonPage: Column(children: [
          ...buttonKeys.map((key) {
            return _buildButton(context, key);
          }).toList(),
        ]),
      ),
    );
  }

  Widget _buildButton(BuildContext context, String key) {
    int index = buttonKeys.indexOf(key);
    return Container(
      margin:
          EdgeInsets.only(bottom: index == buttonKeys.length - 1 ? 0 : 20.0),
      child: CommonButton(
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SymptomDescriptionScreen(
                  symptomKey: key,
                  title: title,
                ),
              ),
            );
          },
          buttonText: tr('quick_help.symptoms.$key.name')),
    );
  }
}
