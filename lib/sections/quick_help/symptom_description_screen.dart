import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:UAWell/custom_widgets/center_text_common_button.dart';

import '../../custom_widgets/common_page_padding.dart';
import '../../custom_widgets/custom_app_bar.dart';
import '../../generated/locale_keys.g.dart';
import '../../styles/styles.dart';
import 'exercise_description_screen.dart';

class SymptomDescriptionScreen extends StatelessWidget {
  final String symptomKey;
  final String title;

  SymptomDescriptionScreen({required this.symptomKey, required this.title});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(titleBar: title),
      body: Container(
        child: CommonPagePadding(
          commonPage: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                tr('quick_help.symptoms.$symptomKey.name'),
                style: TitleDescriptionTextStyles.textStyle,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Text(
                tr('quick_help.symptoms.${symptomKey}.description'),
                style: DescriptionTextStyles.textStyle,
                textAlign: TextAlign.justify,
              ),
              SizedBox(height: 30),
              CenterTextCommonButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ExerciseDescriptionScreen(
                          symptomKey: symptomKey,
                        ),
                      ),
                    );
                  },
                  buttonText: LocaleKeys.common_buttons_start.tr()),
            ],
          ),
        ),
      ),
    );
  }
}
