import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:UAWell/generated/locale_keys.g.dart';

import '../../custom_widgets/logo_widget.dart';
import '../../styles/styles.dart';
import '../about_app/about_app_screen.dart';
import '../contacts/contacts_screen.dart';
import '../language_selection_screen.dart';
import '../therapy_options_screen.dart';

class MenuScreen extends StatefulWidget {
  @override
  _MenuScreenState createState() => _MenuScreenState();
}

class _MenuScreenState extends State<MenuScreen> {


  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xFF3A37AC), //or set color with: Color(0xFF0000FF)
      statusBarIconBrightness: Brightness.light,
    ));
    return Scaffold(
      backgroundColor: Color(0xFF3A37AC), // Синий фон
      body:  Column(
        children: [
          Container(
            alignment: Alignment.centerRight,
            padding: EdgeInsets.only(top: 30, bottom: 20, right: 30),
            child: IconButton(
              visualDensity: VisualDensity.compact,
              alignment: Alignment.centerRight,
              padding: EdgeInsets.zero,
              icon: Image.asset(
                'assets/images/cross_button.png',
                width: 23,
                height: 24,
              ),
              color: Colors.white,
              onPressed: () {
                SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
                  statusBarColor: Color(0xFFFAFAFA), //or set color with: Color(0xFF0000FF)
                  statusBarIconBrightness: Brightness.dark,
                ));
                Navigator.pop(context); // Возврат на предыдущую страницу
              },
              iconSize: 30, // Устанавливаем размер иконки
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  // Логика для начала сначала
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => LanguageSelectionScreen(),
                    ),
                  );
                },
                child: Text(
                  LocaleKeys.language_selection_screen_title.tr().toUpperCase(),
                  style: GoogleFonts.getFont(
                    'Inter',
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 3,
                    height: 1.2,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 40), // Отступ между кнопками
              GestureDetector(
                onTap: () {
                  Navigator.pushReplacementNamed(context, '/TherapyOptionsScreen', arguments: '');
                },
                child: Text(
                  LocaleKeys.therapy_options_screen_title.tr().toUpperCase(),
                  style: GoogleFonts.getFont(
                    'Inter',
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 3,
                    height: 1.2,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 40), // Отступ между кнопками
              GestureDetector(
                onTap: () {
                  // Логика для начала сначала
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ContactsScreen(),
                    ),
                  );
                },
                child: Center(
                  child: Text(
                    LocaleKeys.contacts_screen_title.tr().toUpperCase(),
                    style: GoogleFonts.getFont(
                      'Inter',
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w500,
                      letterSpacing: 3,
                      height: 1.2,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              SizedBox(height: 40), // Отступ между кнопками

              GestureDetector(
                onTap: () {
                  // Логика для начала сначала
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AboutAppScreen(),
                    ),
                  );
                },
                child: Text(
                  LocaleKeys.about_app_screen_title.tr().toUpperCase(),
                  style: MenuButtonTextStyles.textStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 60),
              LogoWidget(textStyle: LogoDarkBackgroundTextStyles.textStyle,),
            ],
          ),
        ],
      ),
    );
  }
}
