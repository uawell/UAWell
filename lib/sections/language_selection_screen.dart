import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import '../custom_widgets/big_text_button.dart';
import '../custom_widgets/custom_app_bar.dart';
import '../custom_widgets/logo_widget.dart';
import '../custom_widgets/start_page_padding.dart';
import '../generated/locale_keys.g.dart';
import '../styles/styles.dart';

class LanguageSelectionScreen extends StatelessWidget {
  final String title = LocaleKeys.language_selection_screen_return_page_title;
  LanguageSelectionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    double screenWidth = MediaQuery.of(context).size.width;
    double buttonWidth = screenWidth - 60;
    double scalingFactor = 0.2;
    double buttonHeight = buttonWidth * scalingFactor;

    return Scaffold(
      backgroundColor: Color(0xFFFAFAFA),

      appBar: const CustomAppBar(titleBar: ''),


      body: StartPagePadding(
          startPage: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              SizedBox(height: 42),
              LogoWidget(textStyle: LogoLightBackgroundTextStyles.textStyle,),
              SizedBox(height: 120),
              BigTextButton(
                onPressed: () {
                  context.setLocale(const Locale('uk'));
                  Navigator.pushNamed(context, '/TherapyOptionsScreen', arguments: title);
                },
                buttonText: 'Український',
              ),
              SizedBox(height: 30),
              BigTextButton(
                onPressed: () {
                  context.setLocale(const Locale('ru'));
                  Navigator.pushNamed(context, '/TherapyOptionsScreen', arguments: title);
                },
                buttonText: 'Русский',
              ),
              SizedBox(height: 30),
              BigTextButton(
                onPressed: () {
                  context.setLocale(const Locale('en'));
                  Navigator.pushNamed(context, '/TherapyOptionsScreen', arguments: title);
                },
                buttonText: 'English',
              ),
            ],
          ),
      ),
    );
  }
}
