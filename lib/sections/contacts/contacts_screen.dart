import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:UAWell/generated/locale_keys.g.dart';

import '../../custom_widgets/center_text_common_button.dart';
import '../../custom_widgets/custom_app_bar.dart';
import '../../styles/styles.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(titleBar: ""),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(30, 28, 30, 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              LocaleKeys.contacts_screen_title.tr(),
              style: TitleDescriptionTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 6),
            Text(
              LocaleKeys.contacts_screen_description.tr(),
              style: DescriptionBlueTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 28),
            _buildContactCard(
              'assets/images/Yana.jpg',
              LocaleKeys.contacts_screen_yana_name.tr(),
              LocaleKeys.contacts_screen_yana_surname.tr(),
              LocaleKeys.contacts_screen_yana_languages.tr(),
              LocaleKeys.contacts_screen_yana_email.tr(),
              LocaleKeys.contacts_screen_yana_youtube.tr(),
              "",
              LocaleKeys.contacts_screen_yana_description.tr(),
            ),
            SizedBox(height: 30),
            _buildContactCard(
              'assets/images/Andrey.png',
              LocaleKeys.contacts_screen_andrey_name.tr(),
              LocaleKeys.contacts_screen_andrey_surname.tr(),
              LocaleKeys.contacts_screen_andrey_languages.tr(),
              LocaleKeys.contacts_screen_andrey_email.tr(),
              LocaleKeys.contacts_screen_andrey_youtube.tr(),
              LocaleKeys.contacts_screen_andrey_site.tr(),
              LocaleKeys.contacts_screen_andrey_description.tr(),
            ),
            SizedBox(height: 30),
            CenterTextCommonButton(
              onPressed: () {
                // Возвращаемся на предыдущую страницу
                Navigator.pop(context);
              },
              buttonText: LocaleKeys.common_buttons_ok.tr(),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildContactCard(String imageAsset,
      String name,
      String familyName,
      String languages,
      String email,
      String youtube,
      String site,
      String description,) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage(imageAsset),
            ),
            SizedBox(width: 13),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  name,
                  style: TitleDescriptionTextStyles.textStyle,
                  textAlign: TextAlign.left,
                ),
                Text(
                  familyName,
                  style: TitleDescriptionTextStyles.textStyle,
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ],
        ),
        SizedBox(height: 21),
        Text(
          LocaleKeys.contacts_screen_language_title.tr(),
          style: ContactsTitlesTextStyles.textStyle,
        ),
        Wrap(
          children: [

            Text(
              languages,
              style: ContactsTextStyles.textStyle,
            ),
          ],
        ),
        Text(LocaleKeys.contacts_screen_contacts_title.tr(),
            style: ContactsTitlesTextStyles.textStyle),
        Wrap(
          children: [
            if (email != "")
              InkWell(
                onTap: () async {
                  String subject = LocaleKeys.contacts_screen_email_message_subject.tr();
                  String body = LocaleKeys.contacts_screen_email_message_template.tr();

                  String emailUrl = "mailto:" +
                      email +
                      "?subject=" +
                      subject +
                      "&body=" +
                      body;

                  _launchURL(emailUrl);

                },
                child: Text(
                  LocaleKeys.contacts_screen_email.tr(),
                  style: ContactsTextStyles.textStyle,
                ),
              ),
            SizedBox(width: 5),
            if (youtube != "")
              InkWell(
                onTap: () {
                  _launchURL("http://www.youtube.com/" + youtube);
                },
                child: Text(
                  LocaleKeys.contacts_screen_youtube.tr(),
                  style: ContactsTextStyles.textStyle,
                ),
              ),
            SizedBox(width: 5),
            if (site != "")
              InkWell(
                onTap: () {
                  _launchURL('https://' + site);
                },
                child: Text(
                  LocaleKeys.contacts_screen_site.tr(),
                  style: ContactsTextStyles.textStyle,
                ),
              ),
          ],
        ),
        SizedBox(height: 7),
        Text(
          description,
          style: DescriptionTextStyles.textStyle,
        ),
      ],
    );
  }

  Future<void> _launchURL(String url) async {
    try {
      await launch(
        url,
        enableJavaScript: true,
      );
    } catch (e) {
      throw 'Could not launch $url';
    }
  }
}