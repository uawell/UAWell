import 'package:dropdown_button3/dropdown_button3.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:UAWell/custom_widgets/center_text_common_button.dart';
import 'package:UAWell/custom_widgets/common_button.dart';
import 'package:UAWell/custom_widgets/custom_app_bar.dart';
import 'package:UAWell/generated/locale_keys.g.dart';
import 'package:UAWell/styles/styles.dart';

import '../../custom_widgets/open_dropdown_menu_button.dart';

class PreventionScreen extends StatefulWidget {
  @override
  _PreventionScreenState createState() => _PreventionScreenState();
}

class _PreventionScreenState extends State<PreventionScreen> {
  String selectedIntensity = LocaleKeys.prevention_screen_intensity_minimum;
  String selectedDuration = LocaleKeys.prevention_screen_duration_two_weeks;

  @override
  Widget build(BuildContext context) {
    final String title = LocaleKeys.prevention_screen_return_page_title;
    final String prevScreenTitle =
        ModalRoute.of(context)?.settings.arguments as String;
    return Scaffold(
      appBar: CustomAppBar(
        titleBar: prevScreenTitle,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(30, 27, 30, 42),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              LocaleKeys.prevention_screen_title.tr(),
              style: TitleDescriptionTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 19),
            Text(
              LocaleKeys.prevention_screen_description.tr(),
              style: DescriptionTextStyles.textStyle,
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 24),
            buildDropdownButton(
              label: LocaleKeys.prevention_screen_duration_title.tr(),
              value: selectedIntensity,
              onChanged: (value) {
                setState(() {
                  selectedIntensity = value;
                });
              },
              options: [
                LocaleKeys.prevention_screen_intensity_minimum,
                LocaleKeys.prevention_screen_intensity_middle,
                LocaleKeys.prevention_screen_intensity_maximum
              ],
            ),
            SizedBox(height: 20),
            buildDropdownButton(
              label: LocaleKeys.prevention_screen_duration_title.tr(),
              value: selectedDuration,
              onChanged: (value) {
                setState(() {
                  selectedDuration = value;
                });
              },
              options: [
                LocaleKeys.prevention_screen_duration_two_weeks,
                LocaleKeys.prevention_screen_duration_three_weeks,
                LocaleKeys.prevention_screen_duration_four_weeks
              ],
            ),
            SizedBox(height: 20),
            CenterTextCommonButton(
              onPressed: () {
                int days = 14;
                if(selectedDuration == "prevention_screen.duration.two_weeks") {
                  days = 14;
                } else if(selectedDuration == "prevention_screen.duration.three_weeks") {
                  days = 21;
                } else if(selectedDuration == "prevention_screen.duration.four_weeks") {
                  days = 28;
                }
                Navigator.pushNamed(context, '/PreventionExerciseScreen',
                    arguments: [title, selectedIntensity, days]);
              },
              buttonText: LocaleKeys.common_buttons_start.tr(),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDropdownButton({
    required String label, // Название выпадающего списка
    required String value, // Текущее выбранное значение
    required Function onChanged, // Обработчик изменения значения
    required List<String> options, // Список опций для выбора
  }) {
    double dropdownButtonWidth = MediaQuery.of(context).size.width - 60;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(label, style: SelectLabelTextStyles.textStyle),
        // Отображение названия выпадающего списка
        SizedBox(height: 10),
        // Небольшой отступ
        Container(
          width: dropdownButtonWidth,
          child: DropdownButtonHideUnderline(
            child: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButton2(
                buttonDecoration: BoxDecoration(
                  border: Border.all(
                    color: Color(0xFF3A37AC), // Цвет границы
                    width: 1, // Ширина границы
                  ),
                  borderRadius: BorderRadius.circular(30),
                ),
                // Цвет выпадающего списка
                value: value,
                // Текущее значение выпадающего списка
                onChanged: (String? newValue) {
                  onChanged(
                      newValue!); // Вызов функции-обработчика при изменении значения
                },
                items: options.map((String option) {
                  return DropdownMenuItem(
                    value: option,
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 28, vertical: 10),
                      child: Text(tr(option + ".name"),
                          style: SelectOptionsTextStyles.textStyle),
                    ), // Отображение текста опции
                  );
                }).toList(),

                buttonHeight: 60,
                dropdownDecoration: BoxDecoration(
                  border: Border.all(
                    color: Color(0xFF3A37AC), // Цвет границы
                    width: 1, // Ширина границы
                  ),
                  borderRadius: BorderRadius.circular(30),
                ),
                offset: const Offset(0, 120),
                customButton: OpenDropDownMenuButton(
                  buttonText: tr(value + '.name'),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
