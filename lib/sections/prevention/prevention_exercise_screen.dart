import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:UAWell/custom_widgets/center_text_common_button.dart';
import 'package:UAWell/generated/locale_keys.g.dart';
import 'package:UAWell/sections/contacts/contacts_screen.dart';
import 'package:UAWell/sections/long_term_work/start_exercise_screen.dart';
import 'package:UAWell/styles/styles.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../custom_widgets/NotificationManagerWidget.dart';
import '../../custom_widgets/common_button.dart';
import '../../custom_widgets/custom_app_bar.dart';

class PreventionExerciseScreen extends StatefulWidget {
  PreventionExerciseScreen();

  @override
  _PreventionExerciseScreenState createState() =>
      _PreventionExerciseScreenState();
}

class _PreventionExerciseScreenState extends State<PreventionExerciseScreen> {

  String exerciseKey = '';
  int selectedDuration = 0;
  bool isSwitchedAlarm = false;
  NotificationHandler notificationHandler = NotificationHandler();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // Вызываем updateState после построения виджета
      updateState(exerciseKey+selectedDuration.toString());
    });
  }

  void updateState(String newExerciseKey) {
    setState(() {
      exerciseKey = newExerciseKey;
      // При изменении exerciseKey, обновите состояние isSwitchedAlarm
      loadSwitchState(exerciseKey);
    });
  }

  Future<void> loadSwitchState(String exerciseKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isSwitchedAlarm = prefs.getBool('isSwitchedAlarm'+exerciseKey) ?? false;
    });
  }

  Future<void> saveSwitchState(bool value, String exerciseKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isSwitchedAlarm'+exerciseKey, value);
  }

  @override
  Widget build(BuildContext context) {

    final List<dynamic> args = ModalRoute.of(context)?.settings.arguments as List<dynamic>;
    final String prevScreenTitle = args[0];
    exerciseKey = args[1];
    selectedDuration = args[2];


    return Scaffold(
      appBar: CustomAppBar(
        titleBar: prevScreenTitle,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(30, 27, 30, 40),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  tr(exerciseKey + '.name'),
                  style: TitleDescriptionTextStyles.textStyle,
                ),
                Row(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: Text(LocaleKeys.common_buttons_alarm.tr()),
                    ),
                    Switch(
                      value: isSwitchedAlarm,
                      onChanged: (value) {
                        setState(() {
                          isSwitchedAlarm = value;
                          saveSwitchState(value, exerciseKey+selectedDuration.toString()); // Сохранение состояния при изменении
                          if (value) {
                            notificationHandler.showScheduledNotifications('1' + exerciseKey+selectedDuration.toString(),'Привет!', 'Время выполнение дыхательного упражнения!', 12, 00, selectedDuration);
                            notificationHandler.showScheduledNotifications('2' + exerciseKey+selectedDuration.toString(),'Привет!', 'Время выполнение дыхательного упражнения!', 15, 00, selectedDuration);
                            notificationHandler.showScheduledNotifications('3' + exerciseKey+selectedDuration.toString(),'Привет!', 'Время выполнение дыхательного упражнения!', 18, 00, selectedDuration);
                            if(exerciseKey == 'prevention_screen.intensity.maximum' || exerciseKey == 'prevention_screen.intensity.middle') {
                              notificationHandler.showScheduledNotifications('4' + exerciseKey+selectedDuration.toString(),'Привет!', 'Время выполнение упражнения на релаксацию!', 21, 00, selectedDuration);
                            }
                          } else {
                            notificationHandler.cancelNotificationsForChannel('1' + exerciseKey+selectedDuration.toString());
                            notificationHandler.cancelNotificationsForChannel('2' + exerciseKey+selectedDuration.toString());
                            notificationHandler.cancelNotificationsForChannel('3' + exerciseKey+selectedDuration.toString());

                            if(exerciseKey == 'prevention_screen.intensity.maximum' || exerciseKey == 'prevention_screen.intensity.middle') {
                              notificationHandler.cancelNotificationsForChannel('4' + exerciseKey+selectedDuration.toString());
                            }
                          }
                        });
                      },
                      activeTrackColor: Color(0xFF3A37AC),
                      activeColor: Color(0xFFFFFFFF),
                      inactiveTrackColor: Color(0xFFAFB1B6),                    ),
                  ],
                ),
              ],
            ),
            SizedBox(height: 20),
            Text(
              tr(exerciseKey + ".description"),
              style: DescriptionTextStyles.textStyle,
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 15),
            CenterTextCommonButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => StartExerciseScreen(longTimeWorkDayKey: LocaleKeys.quick_help_exercise_breathing_2),
                    ),
                  );
                },
                buttonText: LocaleKeys.common_buttons_start.tr()),
            SizedBox(height: 20),
            CenterTextCommonButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ContactsScreen(),
                    ),
                  );
                },
                buttonText: LocaleKeys.common_buttons_contact_specialist.tr()),
          ],
        ),
      ),
    );
  }
}
