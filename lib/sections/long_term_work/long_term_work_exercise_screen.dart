import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:UAWell/custom_widgets/center_text_common_button.dart';
import 'package:UAWell/generated/locale_keys.g.dart';
import 'package:UAWell/sections/contacts/contacts_screen.dart';
import 'package:UAWell/sections/long_term_work/start_exercise_screen.dart';
import 'package:UAWell/styles/styles.dart';

import '../../custom_widgets/NotificationManagerWidget.dart';
import '../../custom_widgets/custom_app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LongTermWorkExerciseScreen extends StatefulWidget {
  LongTermWorkExerciseScreen();

  @override
  _LongTermWorkExerciseScreenState createState() =>
      _LongTermWorkExerciseScreenState();
}

class _LongTermWorkExerciseScreenState
    extends State<LongTermWorkExerciseScreen> {
  String exerciseKey = '';
  bool isSwitchedAlarm = false;
  NotificationHandler notificationHandler = NotificationHandler();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      // Вызываем updateState после построения виджета
      updateState(exerciseKey);
    });
  }

  void updateState(String newExerciseKey) {
    setState(() {
      exerciseKey = newExerciseKey;
      // При изменении exerciseKey, обновите состояние isSwitchedAlarm
      loadSwitchState(exerciseKey);
    });
  }

  Future<void> loadSwitchState(String exerciseKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      isSwitchedAlarm = prefs.getBool('isSwitchedAlarm' + exerciseKey) ?? false;
    });
  }

  Future<void> saveSwitchState(bool value, String exerciseKey) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool('isSwitchedAlarm' + exerciseKey, value);
  }

  @override
  Widget build(BuildContext context) {
    //   final String prevScreenTitle = ModalRoute.of(context)?.settings.arguments as String;

    final List<dynamic> args =
    ModalRoute
        .of(context)
        ?.settings
        .arguments as List<dynamic>;
    final String prevScreenTitle = args[0];
    final int days = args[1];
    if (days == 1) {
      exerciseKey = LocaleKeys.long_time_work_1_day;
    } else if (days == 5) {
      exerciseKey = LocaleKeys.long_time_work_5_day;
    } else if (days == 10) {
      exerciseKey = LocaleKeys.long_time_work_10_day;
    } else if (days == 15) {
      exerciseKey = LocaleKeys.long_time_work_15_day;
    }

    return Scaffold(
      appBar: CustomAppBar(
        titleBar: prevScreenTitle,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(30, 27, 30, 40),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  tr(exerciseKey + '.name'),
                  style: TitleDescriptionTextStyles.textStyle,
                ),
                if (exerciseKey == LocaleKeys.long_time_work_1_day ||
                    exerciseKey == LocaleKeys.long_time_work_5_day)
                  Row(
                    children: [
                      Container(
                        padding: EdgeInsets.all(10.0),
                        child: Text(LocaleKeys.common_buttons_alarm.tr()),
                      ),
                      Switch(
                        value: isSwitchedAlarm,
                        onChanged: (value) {
                          setState(() {
                            isSwitchedAlarm = value;
                            saveSwitchState(value,
                                exerciseKey); // Сохранение состояния при изменении
                            if (value) {
                              if (exerciseKey ==
                                  LocaleKeys.long_time_work_1_day) {
                                notificationHandler.showScheduledNotifications(
                                    '1' + exerciseKey,
                                    'Привет!',
                                    'Время выполнение дыхательного упражнения!',
                                    12,
                                    00,
                                    15);
                                notificationHandler.showScheduledNotifications(
                                    '2' + exerciseKey,
                                    'Привет!',
                                    'Время выполнение дыхательного упражнения!',
                                    15,
                                    00,
                                    15);
                                notificationHandler.showScheduledNotifications(
                                    '3' + exerciseKey, 'Привет!',
                                    'Время выполнение дыхательного упражнения!',
                                    18, 00, 15);
                              } else if (exerciseKey ==
                                  LocaleKeys.long_time_work_5_day) {
                                notificationHandler.showScheduledNotifications(
                                    '4' + exerciseKey, 'Привет!',
                                    'Время выполнение упражнения на релаксацию!',
                                    21, 00, 10);
                              }
                            } else {
                              if (exerciseKey ==
                                  LocaleKeys.long_time_work_1_day) {
                                notificationHandler
                                    .cancelNotificationsForChannel(
                                    '1' + exerciseKey);
                                notificationHandler
                                    .cancelNotificationsForChannel(
                                    '2' + exerciseKey);
                                notificationHandler
                                    .cancelNotificationsForChannel(
                                    '3' + exerciseKey);
                              } else if (exerciseKey ==
                                  LocaleKeys.long_time_work_5_day) {
                                notificationHandler
                                    .cancelNotificationsForChannel(
                                    '4' + exerciseKey);
                              }
                            }
                          });
                        },
                        activeTrackColor: Color(0xFF3A37AC),
                        activeColor: Color(0xFFFFFFFF),
                        inactiveTrackColor: Color(0xFFAFB1B6),
                      ),
                    ],
                  ),
              ],
            ),
            SizedBox(height: 20),
            Text(
              tr(exerciseKey + ".instruction"),
              style: DescriptionTextStyles.textStyle,
              textAlign: TextAlign.left,
            ),
            SizedBox(height: 15),
            if (exerciseKey ==
                LocaleKeys.long_time_work_1_day)
            CenterTextCommonButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) =>
                          StartExerciseScreen(
                              longTimeWorkDayKey:
                              LocaleKeys.quick_help_exercise_breathing_2),
                    ),
                  );
                },
                buttonText: LocaleKeys.common_buttons_start.tr()),
            SizedBox(height: 20),
            CenterTextCommonButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ContactsScreen(),
                    ),
                  );
                },
                buttonText: LocaleKeys.common_buttons_contact_specialist.tr()),
          ],
        ),
      ),
    );
  }
}
