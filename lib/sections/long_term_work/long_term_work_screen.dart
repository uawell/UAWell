import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:UAWell/custom_widgets/common_button.dart';
import 'package:UAWell/custom_widgets/custom_app_bar.dart';
import 'package:UAWell/styles/styles.dart';

import '../../custom_widgets/center_text_common_button.dart';
import '../../generated/locale_keys.g.dart';
import 'long_term_work_exercise_screen.dart';

class LongTermWorkScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final String title = LocaleKeys.long_time_work_return_page_title;
    final String prevScreenTitle = ModalRoute.of(context)?.settings.arguments as String;
    return Scaffold(
      appBar: CustomAppBar(
        titleBar: prevScreenTitle,
      ),
      body: Container(
        child: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(30, 27, 30, 40),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                LocaleKeys.long_time_work_title.tr(),
                style: TitleDescriptionTextStyles.textStyle,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Text(
                LocaleKeys.long_time_work_description.tr(),
                style: DescriptionTextStyles.textStyle,
              ),

              SizedBox(height: 18),
              // Отступ между первой и второй кнопкой
              CommonButton(
                  onPressed: () {
                    // Обработчик для длительной работы
                    Navigator.pushNamed(context, '/LongTermWorkExerciseScreen', arguments: [title, 1]);
                  },
                  buttonText: LocaleKeys.long_time_work_1_day_button.tr()),
              SizedBox(height: 30),
              // Отступ между второй и третьей кнопкой
              CommonButton(
                  onPressed: () {
                    // Обработчик для длительной работы
                    Navigator.pushNamed(context, '/LongTermWorkExerciseScreen', arguments: [title, 5]);
                  },
                  buttonText: LocaleKeys.long_time_work_5_day_button.tr()),
              SizedBox(height: 30),
              // Отступ между третьей и четвертой кнопкой
              CommonButton(
                  onPressed: () {
                    // Обработчик для длительной работы
                    Navigator.pushNamed(context, '/LongTermWorkExerciseScreen', arguments: [title, 10]);
                  },
                  buttonText: LocaleKeys.long_time_work_10_day_button.tr()),
              SizedBox(height: 30),
              // Отступ между четвертой кнопкой и концом столбца
              CommonButton(
                  onPressed: () {
                    // Обработчик для длительной работы
                    Navigator.pushNamed(context, '/LongTermWorkExerciseScreen', arguments: [title, 15]);
                  },
                  buttonText: LocaleKeys.long_time_work_15_day_button.tr()),
              SizedBox(height: 30),
            ],
          ),
        ),
      ),
    );
  }
}
