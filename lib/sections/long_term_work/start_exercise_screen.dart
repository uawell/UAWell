import 'dart:async';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../custom_widgets/breathing_animation_widget.dart';
import '../../custom_widgets/center_text_common_button.dart';
import '../../generated/locale_keys.g.dart';
import '../../styles/styles.dart';

class StartExerciseScreen extends StatefulWidget {
  final String longTimeWorkDayKey;

  StartExerciseScreen({super.key, required this.longTimeWorkDayKey});

  @override
  State<StatefulWidget> createState() => _StartExerciseScreenState();
}

class _StartExerciseScreenState extends State<StartExerciseScreen> {
  bool _isInhale = false;
  Timer? _timer;
  int _elapsedSeconds = 0;
  bool _isTimerRunning = false;
  String _startButton = 'Старт';

  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    return Scaffold(
      appBar: null,
      body: Container(
        child: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(30, 20 + statusBarHeight, 30, 30),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                'assets/images/alarm.png',
                width: 120,
                height: 123,
              ),
              SizedBox(height: 30),
              Text(
                tr(widget.longTimeWorkDayKey + '.description'),
                style: DescriptionTextStyles.textStyle,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20),
              Container(
                width: 210,
                height: 210,
                child: BreathingAnimationWidget(duration: 5, breathFrequency: 600,symptomKey:widget.longTimeWorkDayKey),
              ),
              //  SizedBox(height: 10),
              CenterTextCommonButton(
                onPressed: () {
                  _stopTimer();
                  Navigator.pop(context); // Возврат на предыдущую страницу
                },
                buttonText: LocaleKeys.common_buttons_ok.tr(),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _handleTap() {
    setState(() {
      _isInhale = !_isInhale;
      _startButton = _isInhale
          ? LocaleKeys.start_exercise_widget_inhale.tr()
          : LocaleKeys.start_exercise_widget_exhale.tr();
    });
  }

  void _startTimer() {
    if (!_isTimerRunning) {
      _timer = Timer.periodic(Duration(seconds: 5), (timer) {
        if (_elapsedSeconds >= 600) {
          _stopTimer();
        } else {
          _elapsedSeconds++;
        }
        _handleTap();
      });
      _isTimerRunning = true;
    }
  }

  void _stopTimer() {
    _timer?.cancel();
    _isTimerRunning = false;
    _elapsedSeconds = 0;
  }

  @override
  void dispose() {
    _stopTimer();
    super.dispose();
  }
}
