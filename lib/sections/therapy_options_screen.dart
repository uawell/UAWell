import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../custom_widgets/big_text_button.dart';
import '../custom_widgets/common_page_padding.dart';
import '../custom_widgets/custom_app_bar.dart';
import '../custom_widgets/logo_widget.dart';
import '../custom_widgets/start_page_padding.dart';
import '../generated/locale_keys.g.dart';
import '../styles/styles.dart';

class TherapyOptionsScreen extends StatelessWidget {
  const TherapyOptionsScreen({super.key});

  final String title = LocaleKeys.therapy_options_screen_return_page_title;

  @override
  Widget build(BuildContext context) {
    final String prevScreenTitle =
        ModalRoute.of(context)?.settings.arguments as String;
    return Scaffold(
      appBar: CustomAppBar(titleBar: prevScreenTitle),
      body: StartPagePadding(
        startPage: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 42),
            LogoWidget(textStyle: LogoLightBackgroundTextStyles.textStyle,),
            SizedBox(height: 120),
            BigTextButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/QuickHelpOptionsScreen',
                      arguments: title);
                },
                buttonText: LocaleKeys.therapy_options_screen_quick_help.tr()),
            SizedBox(height: 30),
            BigTextButton(
                onPressed: () {
                  // Обработчик для длительной работы
                  Navigator.pushNamed(context, '/LongTermWorkScreen',
                      arguments: title);
                },
                buttonText:
                    LocaleKeys.therapy_options_screen_long_term_work.tr()),
            SizedBox(height: 30),
            BigTextButton(
                onPressed: () {
                  // Обработчик для профилактики
                  Navigator.pushNamed(context, '/PreventionScreen',
                      arguments: title);
                },
                buttonText: LocaleKeys.therapy_options_screen_prevention.tr()),
          ],
        ),
      ),
    );
  }
}
