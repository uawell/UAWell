import 'package:flutter/material.dart';

class LogoLightBackgroundTextStyles {
  static const TextStyle textStyle = const TextStyle(
    fontFamily: "Inter",
    fontSize: 22,
    fontWeight: FontWeight.w600,
    color: Color(0xFF3A37AC),
    height: 1.25,
    letterSpacing: 0,
  );

}

class LogoDarkBackgroundTextStyles {
  static const TextStyle textStyle = const TextStyle(
    fontFamily: "Inter",
    fontSize: 22,
    fontWeight: FontWeight.w600,
    color: Color(0xffffffff),
    height: 1.25,
    letterSpacing: 0,
  );
}

class DescriptionTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 16,
    color: Color(0xFF19191B),
    fontFamily: 'Inter-Normal',
    fontWeight: FontWeight.w400,
    height: 1.44,
    letterSpacing: -0.1,
    overflow: TextOverflow.visible,
  );
}

class DescriptionBlueTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 16,
    color: Color(0xFF9E9CE4),
    fontFamily: 'Inter-Normal',
    fontWeight: FontWeight.w400,
    height: 1.44,
    letterSpacing: -0.1,
    overflow: TextOverflow.visible,
  );
}

class TitleDescriptionTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 20,
    color: Color(0xFF3A37AC),
    fontFamily: 'Inter-Normal',
    fontWeight: FontWeight.w500,
    height: 1.2,
    letterSpacing: 0,
    overflow: TextOverflow.visible,
  );
}

class CenterTextCommonButtonStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 16,
    color: Color(0xFFFFFFFF),
    fontFamily: 'Inter-Normal',
    fontWeight: FontWeight.w500,
    height: 1.25,
    letterSpacing: 0,
    overflow: TextOverflow.visible,
  );
}

class ContactsTitlesTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 14,
    color: Color(0xFFB6EADA),
    fontFamily: 'Inter-Normal',
    fontWeight: FontWeight.w500,
    height: 1.7,
    letterSpacing: 3,
    overflow: TextOverflow.visible,
  );
}

class ContactsTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 14,
    color: Color(0xFF3A37AC),
    fontFamily: 'Inter-Normal',
    fontWeight: FontWeight.w400,
    height: 1.7,
    letterSpacing: 0,
    overflow: TextOverflow.visible,
  );
}

class TitlePreventionSelectStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 14,
    color: Color(0xFF9E9CE4),
    fontFamily: 'Inter-Normal',
    fontWeight: FontWeight.w400,
    height: 1,
    letterSpacing: 0,
    overflow: TextOverflow.visible,
  );
}

class MenuButtonTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 20,
    color: Colors.white,
    fontFamily: 'Inter',
    fontWeight: FontWeight.w500,
    height: 1.2,
    letterSpacing: 3,
    overflow: TextOverflow.visible,
  );
}

class SelectOptionsTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 20,
    color: Color(0xFF3A37AC),
    fontFamily: 'Inter',
    fontWeight: FontWeight.w500,
    height: 1.2,
    letterSpacing: 0,
    overflow: TextOverflow.visible,
  );
}

class SelectLabelTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 14,
    color: Color(0xFF9E9CE4),
    fontFamily: 'Inter',
    fontWeight: FontWeight.w400,
    height: 1,
    letterSpacing: 0,
    overflow: TextOverflow.visible,
  );
}

class BreathingAnimationTextStyles {
  static const TextStyle textStyle = TextStyle(
    decoration: TextDecoration.none,
    fontSize: 16,
    color: Color(0xFF3A37AC),
    fontFamily: 'Inter',
    fontWeight: FontWeight.w600,
    height: 19.33333396911621/16,
    letterSpacing: 0,
    overflow: TextOverflow.visible,
  );
}
