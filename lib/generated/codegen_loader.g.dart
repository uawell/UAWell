// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>?> load(String path, Locale locale) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "common_buttons": {
    "start": "Start",
    "next": "Next",
    "start_over": "Start over",
    "it_helped": "It helped",
    "contact_specialist": "Contact a specialist",
    "alarm": "Alarm",
    "ok": "OK"
  },
  "language_selection_screen": {
    "return_page_title": "Return to language selection",
    "title": "LANGUAGE SELECTION"
  },
  "therapy_options_screen": {
    "quick_help": "Quick help",
    "long_term_work": "Long-term work",
    "prevention": "Prevention",
    "return_page_title": "Return to help type",
    "title": "TYPE OF HELP"
  },
  "about_app_screen": {
    "title": "About the application",
    "description": "This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.\n\nYou should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>."
  },
  "contacts_screen": {
    "title": "About us and contact with us",
    "description": "Seeking professional help is the best solution in many situations. Here you will find the contacts of the specialists who created this App.",
    "language_title": "LANGUAGES ",
    "contacts_title": "CONTACTS ",
    "yana": {
      "name": "Yana",
      "surname": "Shtager",
      "languages": "Ukraine, Russian, English",
      "description": "Clinical psychologist, body-oriented psychotherapist",
      "email": "shtagerpsiholog@gmail.com",
      "youtube": "channel/UC5BVpYpa650pr5B1iTdqkEQ"
    },
    "andrey": {
      "name": "Andrew",
      "surname": "Fleshel",
      "languages": "Ukraine, Russian, English",
      "description": "Psychotherapist, coach, hypnotherapist, supervisor",
      "email": "fleshelll@gmail.com",
      "youtube": "@AndreiFleshel/featured",
      "site": "fleshel.info"
    },
    "email_message": {
      "subject": "Personal message UAWell",
      "template": "Hello! I would like to know more about the possibility of individual consultation"
    },
    "email": "email",
    "youtube": "youtube",
    "site": "site"
  },
  "prevention_screen": {
    "return_page_title": "Return to options",
    "title": "Prevention",
    "description": "Prevention assumes that you feel good, it is designed to prevent unpleasant conditions, in order to feel more cheerful and relaxed. If you notice that you need practice to deal with a discomfort or problem, you can return to the “quick help” section. Here you can choose the intensity of work — from minimum to maximum, depending on your needs at the moment. Choose the intensity and duration of the program — and proceed to the description of the tasks.",
    "intensity": {
      "title": "Select intensity",
      "minimum": {
        "name": "Minimum intensity",
        "description": "Resonant breathing is one of the simplest yet most effective breathing techniques that works well not only for relieving the effects of long-lasting stress, but also for supporting well-being, especially when used regularly. Unlike other breathing techniques, resonant breathing is best done while sitting. During five minutes, inhale for five seconds and exhale for five seconds, without pauses. A visual cue will help you count down the seconds."
      },
      "middle": {
        "name": "Medium intensity",
        "description": "Start by doing breathing exercises daily. Resonant breathing is one of the simplest yet most effective breathing techniques that works well not only for relieving the effects of long-term stress, but also for maintaining good health, especially when used regularly. Unlike other breathing techniques, resonant breathing is best done while sitting. During five minutes, inhale for five seconds and exhale for five seconds, without pauses. A visual cue will help you count down the seconds. \n\nAdd the relaxation and imagination exercise daily. It is great if you could do it at the same time, but this is not necessary. It will take you ten to fifteen minutes to complete this exercise. Close your eyes and pay attention to bodily sensations. Notice where you feel the tension and direct your attention to that place. Take five to six deep slow breaths without pauses, this will help to relax the muscles. If it is difficult to relax, you can first tighten the muscles for ten to fifteen seconds, while having free breathing. Then imagine a place where you feel comfortable and safe. It’s good if the images appear by themselves, but if nothing comes up, you can draw a several relaxing pictures in your mind and chose the best one. What can you see in this image, what do you hear, feel? What do you want to do? Imagine yourself doing what you really want."
      },
      "maximum": {
        "name": "Maximum intensity",
        "description": "Start by doing breathing exercises daily. Resonant breathing is one of the simplest yet most effective breathing techniques that works well not only for relieving the effects of long-term stress, but also for maintaining good health, especially when used regularly. Unlike other breathing techniques, resonant breathing is best done while sitting. During five minutes, inhale for five seconds and exhale for five seconds, without pauses. A visual cue will help you count down the seconds. \n\nAdd the relaxation and imagination exercise daily. It is great if you could do it at the same time, but this is ot necessary. It will take you ten to fifteen minutes to complete this exercise. Close your eyes and pay attention to bodily sensations. Notice where you feel the tension and direct your attention to that place. Take five to six deep slow breaths without pauses, this will help to relax the muscles. If it is difficult to relax, you can first tighten the muscles for ten to fifteen seconds, while having free breathing. Then imagine a place where you feel comfortable and safe. It’s good if the images appear by themselves, but if nothing comes up, you can draw a several relaxing pictures in your mind and chose the best one. What can you see in this image, what do you hear, feel? What do you want to do? Imagine yourself doing what you really want. \n\nSpecific goals change our attention and perception: we begin to focus more on a constructive agenda and things we can influence. Write a list of goals (choose the time period that suits you — it can be a week, a month or more). If the goal is big, write down the specific tasks that need to be completed to achieve it. If necessary, add a description of the barriers you may encounter and strategies for this case. You can also add a list of external and internal resources that you can apply to achieve this goal (so you have a list consists of three parts: goal, barriers, resources)."
      }
    },
    "duration": {
      "title": "Select duration",
      "two_weeks": "Two weeks",
      "three_weeks": "Three weeks",
      "four_weeks": "Four weeks"
    }
  },
  "quick_help": {
    "return_page_title": "Return to symptoms",
    "symptoms": {
      "apathy_empty": {
        "name": "apathy, emptiness, indifference",
        "description": "Apathy implies a lack of desire and strength to do anything, a lack of motivation, emotional emptiness and, as a result, a decrease in productivity to the point of complete impossibility to act. You may feel like you don`t want or can`t do anything because there is no strength or meaning. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one"
      },
      "anxiety_agitation": {
        "name": "anxiety, disturbing fuss",
        "description": "Anxiety is a variant of the normal response to a real or perceived threat. Anxiety is often accompanied by mobilization (activation of the mind and body to avoid a threat), which does not always lead to productive actions. Especially in those situations where it is impossible to do something useful (in situations of helplessness). You may feel like you can`t sit still, think clearly, and act effectively because of anxiety. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "derealization_depersonalization": {
        "name": "Derealization, depersonalization",
        "description": "Derealization is a change in perception, in which the world around us seems not the same as before, unreal, distant, more like a dream or a two-dimensional image. Depersonalization involves the feeling of a change in one`s own personality. You may feel that you have changed, become different from before, that it is not you, that you cannot feel, perceive and react in the same way as before. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "difficulty_breathing": {
        "name": "Obstructed breathing, incomplete breathing",
        "description": "Stress, especially strong or prolonged, significantly affects the pattern of breathing. You may feel that your breathing has become shallow, insufficient, you cannot take a full breath or a full exhalation, that you do not have enough air. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "tension_relaxation_impossibility": {
        "name": "Tension, inability to relax",
        "description": "Stress, especially strong or prolonged, often has a noticeable effect on muscle tone. You may feel that you are overly tense and unable to relax, or that it is possible to relax, but then the tension quickly returns. Sometimes this leads to muscle pain or reduced mobility. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "unpleasant_physical_reactions": {
        "name": "Unpleasant bodily reactions",
        "description": "Stress can cause various unpleasant bodily reactions, such as nausea, numbness, chills, fever, dizziness, etc. These reactions can be more or less severe and prolonged. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "panic": {
        "name": "Panic",
        "description": "Panic is an attack of intense anxiety that usually lasts from several minutes to up to an hour and involves intense bodily reactions — dizziness, muscle tension or weakness, a change in the sensation of body temperature, palpitations, etc. Specific thoughts are also possible — that this condition is dangerous, it portends fainting, heart attack, unhealthy behaviour, insanity or death. Panic attacks are safe for mental and physical health condition, although they are very unpleasant. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "immersion_painful_experience_flashback_traumatic_funnel": {
        "name": "Immersion in painful experience\n(“flashback”, “traumatic funnel”)",
        "description": "Immersion in a painful experience is most often a response to a trigger (something that resembles a traumatic circumstance — a sound, an event, a smell, etc.), but can also occur on its own. At this moment, the psyche reacts as if the danger from the past has returned. Such reactions are accompanied by strong emotions and behavioural changes (freezing or actions that correspond to the perceived danger). Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "bewilderment_inability_concentrate": {
        "name": "Confusion, inability to concentrate",
        "description": "Confusion can be a more or less intense reaction that occurs as a response to stress, overload. You may feel like you don’t understand what is happening, you can’t concentrate, you don’t know what to do, what decision to make, how to respond to the interlocutor, etc. Click on the “start” button and you will eceive a series of simple instructions that will help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "intense_unbearable_emotions": {
        "name": "Strong, unbearable emotions",
        "description": "Strong emotions are generally more difficult to bear than weak or moderate ones, a situation of prolonged or intense stress, even relatively weak emotions can be felt as unbearable. You may feel that you cannot stand the emotional intensity, that emotions cover you, do not allow you to think clearly and act effectively. Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "weakness_helplessness": {
        "name": "Weakness, powerlessness",
        "description": "Powerlessness is a variant of an emotional reaction that affects behaviour in different ways. Experiencing powerlessness, you can either do nothing or act as before, while feeling that you are exhausted, unable to cope and cannot influence the situation. Click on the “start” button” and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "shock_stupor_freeze": {
        "name": "Shock, stupefaction, torpor",
        "description": "Shock and freeze are defensive reactions that occur as a response to overload, overly intense emotions and sensations. They can be more or less pronounced, from feeling emotionally “numb” to an inability to respond normally to external stimulus (sounds, people talking, etc.), a complete inability to feel one’s body, including pain (dissociation) and inability to act (stupor). Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      },
      "emotional_thoughts": {
        "name": "Emotional thoughts",
        "description": "Thinking changes quite easily under stress and the influence of strong emotions, especially in a situation where it is impossible to influence the source of discomfort. Thoughts become more categorical, reflecting only one side of reality or frankly irrational. “It’s all my fault”, “Everything is lost”, etc. Are typical examples of such ideas. A special case is thoughts of suicide caused by strong emotions, when the goal is not to end life, but to alleviate suffering. If you cannot cope with thoughts of death, please seek help from a specialist (click on the “contact a specialist” button that appears after all the instructions). Click on the “start” button and you will receive a series of simple instructions to help you improve your well-being. Proceed to the next instruction only after completing the previous one."
      }
    }
  },
  "quick_help_exercise": {
    "breathing_1": {
      "name": "Breathing",
      "description": "Take five to twenty full breaths, similar to a natural sigh of relief: a full breath with the whole body — so that the breath includes the stomach, diaphragm (when you breathe as if with your back), ribs and chest — and a relaxed noisy exhalation. Take such breaths in a row, without pauses, counting them in your mind or bending your fingers. It is best to breathe through your mouth, so it will be easier to relax. Breathe at your own pace, slowly, so that you have time to relax on each exhalation. If you can’t immediately take a free breath, it’s okay, over time, the tension will decrease, a skill will form and you will breathe freely. You may feel dizzy or have other effects of hyperventilation (numbness in the muscles, especially in the face and hands, tingling, etc.). If it’s uncomfortable, slow down; if it’s not uncomfortable, keep going. You can take more than twenty breaths — until you feel that the state has changed, you have become easier and more comfortable."
    },
    "physical_exercise_1": {
      "name": "Attention to sensations, grounding, relaxation",
      "description": "Pay attention to bodily sensations. Notice where there is tension in the body, where you feel any sensations, what is pleasant and what is not. Directed attention can be combined with free breathing (full inhalation — relaxed exhalation slowly, but without pauses). Notice how relaxed your neck is, and if you feel tension, relax your neck: the easiest way is to shake your head in different directions, gently and slowly, for a minute or two. If there is tension in the body, direct your attention to the tense place, fix it and breathe freely three to five times. Exhaling naturally helps to relax the muscles. As you relax more, pay attention to the sensations in your legs. If you poorly feel your feet, move your toes, try to catch the floor with them (even if you are wearing shoes). Feel the support under your feet, notice the sensations of contact with the support. This can also be combined with free breathing (five to ten breaths, slowly, without pauses)."
    },
    "physical_exercise_2": {
      "name": "Stretching",
      "description": "Pay attention to bodily sensations. Notice where in the body there is tension, and where, on the contrary, muscle weakness. Stretch several times as you want, as it will be pleasant and comfortable. Combine sipping with free breathing (five to ten breaths slowly, without pauses). Do this for as long as it takes to feel that the state has changed, the body has come to life and relaxed — or until it is pleasant and interesting for you to continue."
    },
    "physical_exercise_3": {
      "name": "Yawning",
      "description": "Intentional yawning allows you quickly relieve excessive tension. In order to yawn at will, relax your jaw, inhale as completely as possible (not trying too hard and slowly, but so that the inhalation is really full, for the entire volume of the lungs) — and then, without exhaling, inhale a little more, and more a little while it orks. If you don’t manage to do it right away, it’s okay, trying also has a relaxing effect. Most likely, at some point you will be able to yawn arbitrarily. Repeat this five to seven times."
    },
    "breathing_2": {
      "name": "Breathing",
      "description": "Resonant breathing is one of the simplest yet effective breathing techniques that works well for relieving the effects of long-term stress, especially when used regularly. Unlike other breathing techniques, resonant breathing is best done while sitting. For five minutes, inhale for five seconds and exhale for five seconds, without pauses. A visual cue will help you count down the seconds."
    },
    "breathing_3": {
      "name": "Breathing",
      "description": "The alternation of fast and slow breathing helps to quickly change both the emotional and bodily state due to the physiology of breathing, regardless of the thoughts that arise in the process, and also\nregardless of the type of stressor. Do five breathing cycles in this way: each cycle includes four quick breaths (inhale for one second — exhale for one second without pauses), and one slow (inhale for four seconds — exhale for four seconds without pauses). Thus, you will get twenty-five breaths. You will most likely experience dizziness and other effects of hyperventilation (numbness and tingling in the muscles, most commonly in the arms and face). These are normal effects that will go away on their own after the exercise is completed. A visual cue will help you count down the seconds."
    },
    "breathing_4": {
      "name": "Breathing",
      "description": "Slow breathing has a calming effect, but slowing down breathing immediately is often difficult due to muscle tension. Therefore, it is easier to gradually slow down breathing, and at the same time slowly relax tense muscles. Inhale for one second — exhale for two, then inhale for three — exhale for four, inhale for five — exhale for six, etc. up to sixteen seconds. If desired and if possible, you can extend the exercise (inhale for seventeen seconds — exhale for eighteen). A visual cue will help you count down the seconds."
    },
    "breathing_5": {
      "name": "Breathing",
      "description": "This breathing exercise is quite intense, so if — especially at the beggining — the effects are too strong for you, you can skip this step and come back to it later. Take twenty quick breaths (inhale for one second — exhale for one second without pauses) — then pause for thirty seconds. After a pause, inhale — and hold your breath for another fifteen seconds. After that, take another twenty quick breaths (same as before) — then pause for a minute. After a pause, inhale — and hold your breath for another thirty seconds. To finish, take another twenty quick breaths, then pause for a minute and a half. After a pause, inhale — and hold your breath for another forty-five seconds. If the pauses are too long for you and you want to inhale earlier, this can be done. Be aware the exercise is really intense and can cause an emotional outburst. It’s acceptable, but if it seems too intense, it’s best to skip it or put it off. When you’re ready to start, click on the circle below the instructions."
    },
    "containment": {
      "name": "Containing",
      "description": "The physiology of emotions is such that it does not allow us to arbitrarily choose when an emotional response begins or ends, but we choose behavioural responses to emotions. At the level of behaviour, we can choose to suppress emotions — to take control of ourselves, not to show emotions externally, not to give them free rein internally. This allows us to save face and sometimes security (when emotions frighten us with their force), but has delayed adverse effects. We can also choose expression — a more or less violent expression of emotions, which is more rational in terms of the impact on health, but less — in terms of influencing our image in our own eyes and in the eyes of people around us. The third option — containment (or, in other terms, living, withstanding, confrontation) involves maintaining contact with emotion and living it (the emotion goes through its natural cycle) without losing control and undesirable external manifestations. So, containerization should be done like this:\n\n1. Name the feeling that gripped you. If it is too complicated to name, try to choose the most appropriate option from the list (only negative emotions and feelings will be presented in it, because there are usually no difficulties with positive ones): \n\n\t- sadness (sorrow, grief, despair, mourning, depression, melancholy, despondency)\n\t- anger (irritation, indignation, fury, hatred, wrath, rage)\n\t- fear (anxiety, concern, unrest, disturbance, agitation, panic)\n\t- shame\n\t- guilt\n\t- hurt (offence)\n\t- envy\n\t- jealousy\n\t- powerlessness\n\t- confusion\n\t- disgust\n\t- pain\n\n2. Name the reason that caused this feeling. This point is optional for containment, however often linking the feeling to the cause is soothing and helps to feel relieved. \n\n3. Pay attention to bodily sensations and look for reactions in the body associated with this feeling.\n\n4. Breathe freely several times (as long as it takes to feel better): full breath — relaxed, noisy exhalation. While breathing, keep your attention on bodily sensations, they are the markers that you can focus on when choosing the duration of the exercise."
    },
    "imagery_rehearsal": {
      "name": "Reaction in imagination",
      "description": "Experiencing the impact of any stressor, the psyche begins to look for ways to get rid of it or eliminate its source. The most famous reactions are fight or flight, but in addition to the “fight” or “flight” impulses, we can also experience others the simplest and not always meaningful actions. In most cases, it is impossible to actually realize such an impulse (usually due to social restrictions or physical ones), and its suppression increases stress. Pay attention to bodily sensations. What does your body want to do — hit, run, hide, cry, scream, close, grab something or someone, push, throw something, etc.? Release your breath (take a few breaths or breathe deeply, slowly and without pauses). Imagine yourself doing the action that the body wants. Imagine this for as long as it takes for the body to relax and the emotions to calm down.\n\nImportant! Many people fear that if they imagine aggressive and destructive actions (such as fighting or breaking things), they will lose control of their behaviour and actually do something dangerous. Such anxiety is understandable, but unreasonable — we distinguish between imagination and reality and realize that in the imagination something is possible that is impossible in reality."
    },
    "attention_management": {
      "name": "Managing attention",
      "description": "Pay attention to your surroundings. What do you see in front of you? Name five objects and what they are made of (for example, “this is a wooden chair”, “this is a glass” cup, “this is a cardboard box”, etc.). Then find and name three objects made of wood (or metal, plastic, fabric — depending on what is around). Name three things that are red (or blue/green if there is no red). Name three round objects (square, triangular). If you feel like you want to continue, select three more options (color, shape, material) and do the exercise again or several times."
    },
    "normalization_and_positive_meaning_assignment": {
      "name": "Normalization and determination of the positive meaning of the symptom",
      "description": "All unpleasant emotions, sensations and thoughts that arising from stress are a consequence of the psyche attempts to adapt to the world around us. The stimuli that affect us are sometimes too strong to adapt to without discomfort. In this case, we tend to be afraid of our own reactions and are dissatisfied with the feelings, thoughts and other manifestations that we have. These so-called secondary reactions only increase stress. In such cases, normalization of mental reactions helps to feel better. Try to identify and name the function that your “symptom” performs (what you do not like and what you would not like to feel and experience). For example, the inability to relax, irritability, or difficulty falling asleep is the heightened vigilance required for safety. Trembling or a panic attack is a way to reduce the accumulated tension. Nightmares are a tool for processing unpleasant and painful experiences, etc. Think about what “goals and tasks” your psyche wanted to solve, producing an unpleasant reaction for you? Even if the answer does not come immediately, this way you will switch your attention to a more constructive focus."
    },
    "somatic_knot": {
      "name": "Corporeal node",
      "description": "In many cases, planning actions helps to calm down and feel relief — especially if these actions help to cope with the stressor that caused the reaction. The technique for such planning is best done in writing, but if you are in a place where it is impossible to write, just think over the answers in your mind.\n\n1. Fact. Name the fact that caused you an unpleasant reaction. A fact is something that can be seen and heard, not our thoughts on it. For example, “he/she/they said/did” — and then a direct quote or description of the action.\n\n2. Interpretation. What did you think about what happened? What did this mean for you? What meaning do you see in the described event?\n\n3. Emotion. Name the emotion that the event caused (most likely an interpretation, not the fact itself). It can be sadness, anger, fear, resentment, guilt, shame, confusion, powerlessness, disgust, pain — or some combination of feelings.\n\n4. Feeling. Notice the sensations in your body. Where do you notice the reaction to this emotion — and what is it? If you release your breath (take a few breaths or start breathing deeply, slowly and without pauses) — doing this practice will be easier and more comfortable.\n\n5. Impulse. What do you want to do in connection with this emotion and its experience at the level of sensations? Impulse is always the simplest possible action (hitting, running, crying, screaming, hiding, closing, pushing, grabbing, etc.). If the desire is strong, pause for two or three minutes, release your breath and imagine how you are doing this action. \n\n6. Options for action. Write down all the options for action that are possible in this situation. Not all of them can be realistic, possible or constructive — the process of generating ideas is important, not an instant search for a solution.\n\n7. Choice of action. Choose what you are really going to do. It could be an option from a list, a combination of options, something new. You can also choose to do nothing or postpone the decision."
    },
    "visualization": {
      "name": "Visualizations",
      "description": "Before starting, it is important to say that if you fail to perform this technique the first time, it’s okay. You can try it again after a while. It is advisable to perform it in a place where for ten to fifteen minutes you will not be disturbed and distracted. Doing this practice requires you to close your eyes, so read the entire instruction first and then start.\n\nClose your eyes and pay attention to bodily sensations. If you notice that some muscles are tense, direct your attention to these places and take a few free breaths (slow deep breath — relaxed breath). Imagine a place where it is calm and safe. It can be a room, a landscape, or even a single planet. Consider what’s in there. Try to let this image develop on its own, so that the elements appear independently of you, but if nothing comes, you can model the image yourself. What do you see, hear, feel in this place? What do you want to do? Do you need someone else, or is it best to be there alone? Imagine that you are doing what you want. Maintain this image for as long as necessary for relief and recuperation."
    }
  },
  "long_time_work": {
    "title": "Long-term work",
    "return_page_title": "Return to day selection",
    "description": "A long-term work is suitable for you if you have experienced prolonged stress and need recovery. You will get the maximum result if you do all the tasks, but if you are not able, do as much as you can, and if necessary, you can go through the whole cycle again. To complete tasks, it is best to have a separate notebook (it is better if you write on paper, or type it if you prefer typing). Start from the first day and continue to complete tasks, adding new ones every few days.",
    "1_day": {
      "name": "First \nday",
      "button": "First day",
      "instruction": "Start with a self-assessment of your condition. Write down everything that worries you in one column. These can be unpleasant body sensations, negative emotions (or, for example, emotional instability, frequent mood swings), unpleasant thoughts, or unwanted behavior. Every evening evaluate the strength of eachindicator. The easiest way to rate this is on a scale of 0 to 2, where 0 means that you didn’t feel it today, 1 — you had moderate feeling, and 2 — if you felt it very strongly. This analysis helps to understand both: which symptoms you need to pay more attention to, and also to see how your condition changes as you work to improve it. \n\nDo breathing exercises daily. Practice resonant breathing three times a day for five minutes (inhale for five seconds — exhale for five seconds without pauses, if this time is too long and breathing is so uncomfortable or impossible — inhale and exhale for four seconds, or three). It is best to do this practice while sitting."
    },
    "5_day": {
      "name": "Fifth \nday",
      "button": "Fifth day",
      "instruction": "Do the relaxation and imagination exercise daily. It is great if you could do it at the same time, but this is not necessary. It will take you ten to fifteen minutes to complete this exercise. Close your eyes and pay attention to bodily sensations. Notice where you feel the tension and direct your attention to that place. Take five to six deep slow breaths without pauses, this will help to relax the muscles. If it is difficult to relax, you can first tighten the muscles for ten to fifteen seconds, while having free breathing. Then imagine a place where you feel comfortable and safe and relax. It’s good if the images appear by themselves, but if nothing comes up, you can draw a several relaxing pictures in your mind and chose the best one. What can you see in this image, what do you hear, feel? What do you want to do? Imagine yourself doing what you reallly want. \n\nThe next exercise is created for the time when you feel that your condition has worsened. Describe what you are experiencing: what emotions, feelings, sensations, thoughts and impulses? Then write down at least three factors (four or five would be better) that led to a deterioration in well-being. It can be events, news, health problems, fatigue, or even a change in the weather. Under each of the factors described, write down what you can do: if the problem can be fixed, then what you will do, if not, then how you can compensate for the troubles or support yourself. Important! If you feel that this is not enough, go back to the “quick help” section."
    },
    "10_day": {
      "name": "Tenth \nday",
      "button": "Tenth day",
      "instruction": "Specific goals change our attention and perception: we begin to focus more on a constructive agenda and things we can influence. Write a list of goals (choose the time of goal realization that suits you — it can be a week, a month or more). If the goal is big, write down the specific tasks that need to be completed to achieve it. If necessary, add a description of the barriers/issues you may encounter and strategies for this case. You can also add a list of external and internal resources that you can apply to achieve this goal (so you have a list consists of three parts: goal, barriers, resources).\n\nRoutine is an important component of self-care and mental health. Pick three enjoyable activities you can do every day. Even if it is something insignificant, doesn’t matter, it is important that it gives you pleasure (or at least lead to it). It can be communication, food, sports, music, walks, etc. Try to add something new each day, but if something repeats, still do that."
    },
    "15_day": {
      "name": "Fifteenth \nday",
      "button": "Fifteenth day",
      "instruction": "Analyze how balanced your daily loads are. All loads conditionally can be divided into physical, emotional (communication, art, news, etc.) and intellectual (work, study, etc.). If there is an area that clearly takes less attention and time, plan how you can balance it. You can remove something from the “overloaded” sphere (if possible) or add something to the desolate sphere. \n\nPay attention to what new positive experience you can bring into your life. It is desirable that it would be something pleasant, enjoyable (new food, new places or routes, new music, etc.), but sometimes it can be something neutral. We need new experience for many reasons, but if this task is difficult to complete right away, start by paying attention to where and how to get a new pleasant experience would be possible. From this step it will be easier to move on to practice."
    }
  },
  "start_exercise_widget": {
    "start": "start",
    "pause": "pause",
    "inhale": "inhale",
    "exhale": "exhale"
  }
};
static const Map<String,dynamic> uk = {
  "common_buttons": {
    "start": "Почати",
    "next": "Далі",
    "start_over": "почати спочатку",
    "it_helped": "Це допомогло",
    "contact_specialist": "Зв’язатися зі спеціалістом",
    "alarm": "Будильник",
    "ok": "ОК"
  },
  "language_selection_screen": {
    "return_page_title": "Повернутися до вибору мови",
    "title": "ВИБІР МОВИ"
  },
  "therapy_options_screen": {
    "quick_help": "Швидка допомога",
    "long_term_work": "Тривала робота",
    "prevention": "Профілактика",
    "return_page_title": "Повернутись до типу допомоги",
    "title": "ТИП ДОПОМОГИ"
  },
  "about_app_screen": {
    "title": "О приложении",
    "description": "Ця програма -- вільне програмне забезпечення: ви можете розповсюджувати її та/або вносити зміни відповідно до умов Загальної Публічної Ліцензії ГНУ у тому вигляді, у якому вона була опублікована Фундацією Вільного Програмного Забезпечення; 3ї версії Ліцензії, або (на Ваш розсуд) будь-якої більш пізньої версії.\n\nЦя програма розповсюджується із сподіванням, що вона виявиться корисною, але БЕЗ БУДЬ-ЯКОЇ навіть УЯВНОЇ ҐАРАНТІЇ КОМЕРЦІЙНОЇ ПРИДАТНОСТІ чи ВІДПОВІДНОСТІ БУДЬ-ЯКОМУ ПЕВНОМУ ЗАСТОСУВАННЮ. Зверніться до Загальної Публічної Ліцензії ГНУ за подробицями.\n\nВи мали отримати копію Загальної Публічної Ліцензії ГНУ разом з цією програмою. Якщо ви не отримали копії ліцензії, див.  <https://www.gnu.org/licenses/>."
  },
  "contacts_screen": {
    "title": "Про нас та зв'язки з нами",
    "description": "Звернутися за професійною допомогою — найкраще рішення у багатьох ситуаціях. Тут Ви знайдете контакти спеціалістів, що створили цей додаток.",
    "language_title": "МОВИ ",
    "contacts_title": "КОНТАКТИ ",
    "yana": {
      "name": "Яна",
      "surname": "Штагер",
      "languages": "Український Російський Англійський",
      "description": "Clinical psychologist, body-oriented psychotherapist",
      "email": "shtagerpsiholog@gmail.com",
      "youtube": "UC5BVpYpa650pr5B1iTdqkEQ"
    },
    "andrey": {
      "name": "Андрій",
      "surname": "Флешель",
      "languages": "Український Російський Англійський",
      "description": "Психотерапевт, коуч, гіпнотерапевт, супервізор",
      "email": "fleshelll@gmail.com",
      "youtube": "@AndreiFleshel/featured",
      "site": "fleshel.info"
    },
    "email_message": {
      "subject": "Особисте повідомлення UaWell",
      "template": "Вітаю! Я хочу дізнатися більше про можливість індивідуальної консультації"
    },
    "email": "email",
    "youtube": "youtube",
    "site": "site"
  },
  "prevention_screen": {
    "return_page_title": "Повернутись до параметрів",
    "title": "Профілактика",
    "description": "Профілактика передбачає, що Ви почуваєтеся добре, вона призначена для попередження неприємних станів, для того, аби відчувати себе більш бадьоро і вільно. Якщо Ви помітите, що Вам необхідні практики роботи з дискомфортом чи проблемою, Ви можете повернутися до розділу “швидка допомога”. Тут Ви можете обрати інтенсивність роботи — від мінімальної до максимальної, залежно від Ваших потреб на даний момент. Оберіть інтенсивність програми — і переходьте до опису завдань.",
    "intensity": {
      "title": "Виберіть інтенсивність",
      "minimum": {
        "name": "Мінімальна",
        "description": "Резонансне дихання — одна з найпростіших, проте ефективних дихальних технік, що добре працює не лише для усунення наслідків тривалого стресу, а і для  підтримки хорошого самопочуття, особливо за регулярного застосування. На відміну від інших дихальних технік, резонансне дихання найкраще робити сидячи. Протягом п’яти хвилин робіть вдих на п’ять секунд і видих на п’ять секунд, без пауз. Візуальна підказка допоможе Вам рахувати секунди."
      },
      "middle": {
        "name": "Середня",
        "description": "Почніть з того, щоб виконувати дихальні вправи щоденно. Резонансне дихання — одна з найпростіших, проте ефективних дихальних технік, що добре працює не лише для усунення наслідків тривалого стресу, а і для  підтримки хорошого самопочуття, особливо за регулярного застосування. На відміну від інших дихальних технік, резонансне дихання найкраще робити сидячи. Протягом п’яти хвилин робіть вдих на п’ять секунд і видих на п’ять секунд, без пауз. Візуальна підказка допоможе Вам рахувати секунди. \n\nДодайте вправу з релаксації та роботи з уявою. Найкраще, якщо Ви виконуватимете їх в один і той самий час, проте це не обов’язково. На виконання цієї вправи Вам знадобиться десять-п’ятнадцять хвилин. Заплющіть очі і зверніть увагу на тілесні відчуття. Помітьте, де Ви відчуваєте напругу — і спрямуйте туди увагу. Зробіть п’ять-шість глибоких повільних зітхань без пауз, це допоможе розслабити м’язи. Якщо розслабитися важко, можна спочатку напружити м’язи на десять-п’ятнадцять секунд, зберігаючи при цьому вільне дихання. Уявіть собі місце, де Вам приємно і безпечно. Добре, якщо образи приходять нібито самі, але якщо нічого не з’являється — Ви можете змоделювати картинку чи перебрати декілька варіантів. Що Ви бачите у цьому образі, що чуєте, відчуваєте? Що хочеться робити? Уявіть, як Ви робите те, що хочеться."
      },
      "maximum": {
        "name": "Максимальна",
        "description": "Почніть з того, щоб виконувати дихальні вправи щоденно. Резонансне дихання — одна з найпростіших, проте ефективних дихальних технік, що добре працює не лише для усунення наслідків тривалого стресу, а і для  підтримки хорошого самопочуття, особливо за регулярного застосування. На відміну від інших дихальних технік, резонансне дихання найкраще робити сидячи. Протягом п’яти хвилин робіть вдих на п’ять секунд і видих на п’ять секунд, без пауз. Візуальна підказка допоможе Вам рахувати секунди. \n\nДодайте вправу з релаксації та роботи з уявою. Найкраще, якщо Ви виконуватимете їх в один і той самий час, проте це не обов’язково. На виконання цієї вправи Вам знадобиться десять-п’ятнадцять хвилин. Заплющіть очі і зверніть увагу на тілесні відчуття. Помітьте, де Ви відчуваєте напругу — і спрямуйте туди увагу. Зробіть п’ять-шість глибоких повільних зітхань без пауз, це допоможе розслабити м’язи. Якщо розслабитися важко, можна спочатку напружити м’язи на десять-п’ятнадцять секунд, зберігаючи при цьому вільне дихання. Уявіть собі місце, де Вам приємно і безпечно. Добре, якщо образи приходять нібито самі, але якщо нічого не з’являється — Ви можете змоделювати картинку чи перебрати декілька варіантів. Що Ви бачите у цьому образі, що чуєте, відчуваєте? Що хочеться робити? Уявіть, як Ви робите те, що хочеться. \n\nКонкретні цілі змінюють нашу увагу і сприйняття: ми починаємо більше фокусуватися на конструктивній повістці і тому, на що ми можемо вплинути. Напишіть список цілей (оберіть той термін, що Вам підходить — це може бути тиждень, місяць чи більше). Якщо ціль велика — розпишіть конкретні задачі, котрі треба виконати для її досягнення. Якщо треба — додайте список бар’єрів, з якими Ви можете стикнутися, а також стратегії на цей випадок. Також можна додати список внутрішніх і зовнішніх ресурсів, до яких можна звернутися для досягнення цієї цілі (тоді у Вас вийде список з трьох частин: ціль, бар’єри, ресурси)."
      }
    },
    "duration": {
      "title": "Виберіть тривалість",
      "two_weeks": "Два тижня",
      "three_weeks": "Три тижні",
      "four_weeks": "Чотири тижні"
    }
  },
  "quick_help": {
    "return_page_title": "Повернутись до симптомів",
    "symptoms": {
      "apathy_empty": {
        "name": "Апатія, спустошеність",
        "description": "Апатія передбачає відсутність бажання і сил щось робити, недостатність мотивації, емоційну спустошеність і як наслідок — зменшення продуктивності аж до повної неможливості діяти. Ви можете відчувати, що нічого не хочете чи не можете, тому що немає сил або сенсу. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "anxiety_agitation": {
        "name": "Занепокоєння, тривога",
        "description": "Тривога — це варіант нормальної реакції на реальну чи передбачувану загрозу. Тривога часто супроводжується активацією (мобілізацією психіки і тіла, аби уникнути загрози), яка не завжди призводить до продуктивних дій. Особливо — у ситуаціях, де зробити щось корисне неможливо (у ситуаціях безсилля). Ви можете відчувати, що через занепокоєння не можете всидіти на місці, ясно мислити та ефективно діяти. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "derealization_depersonalization": {
        "name": "Дереалізація, деперсоналізація",
        "description": "Дереалізація — це зміна сприйняття, за якої навколишній світ здається не таким, як раніше, несправжнім, далеким, більше схожим на сон чи двомірне зображення. Деперсоналізація передбачає відчуття зміни власної особистості. Ви можете відчувати, що змінилися, стали не такими, як раніше, що це не Ви, або що не виходить відчувати та реагувати, як раніше. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "difficulty_breathing": {
        "name": "Важке дихання, неповний вдих",
        "description": "Стрес, особливо сильний або тривалий, відчутно впливає на характер дихання. Ви можете відчувати, що дихання стало поверхневим, недостатнім, не виходить зробити повний вдих чи повний видих, що Вам не вистачає повітря. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "tension_relaxation_impossibility": {
        "name": "Напруження, неможливість розслабитися",
        "description": "Стрес, особливо сильний або тривалий, відчутно впливає на м’язовий тонус. Ви можете відчувати надмірну напругу, неможливість розслабитися, або що Ви можете розслабитися, але потім напруга швидко повертається. Іноді це призводить до болю у м’язах чи зменшенню рухливості. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "unpleasant_physical_reactions": {
        "name": "Неприємні тілесні реакції",
        "description": "Стрес може викликати різноманітні неприємні тілесні реакції, наприклад, нудоту, заніміння, відчуття холоду чи жару, запаморочення тощо. Ці реакції можуть бути більш чи менш сильними і тривалими. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "panic": {
        "name": "Паніка",
        "description": "Паніка — це напад сильної тривоги, який зазвичай триває від декількох до декількох десятків хвилин і передбачає інтенсивні тілесні реакції — запаморочення, напругу чи слабкість у м’язах, відчуття зміни температури тіла, прискорене серцебиття тощо. Також можливі характерні думки — про небезпеку цього стану, про можливу втрату свідомості, загрозу серцевого нападу, божевілля чи смерті. Панічні напади безпечні для психічного і фізичного здоров’я, хоча і дуже неприємні. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "immersion_painful_experience_flashback_traumatic_funnel": {
        "name": "Занурення у болісний досвід («флешбек», «травматична воронка»)",
        "description": "Занурення у болісний досвід найчастіше виявляється реакцією на тригер (щось, що нагадує травматичні обставини — звук, подія, запах тощо), але може відбуватися і саме по собі. У цей момент психіка реагує так, ніби небезпека з минулого повернулася. Такі реакції супроводжуються сильними емоціями і зміною поведінки (завмирання або дії, що відповідають небезпеці). Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "bewilderment_inability_concentrate": {
        "name": "Розгубленість, неможливість зосередитися",
        "description": "Розгубленість може бути більш чи менш інтенсивною реакцією, що виникає як відповідь на стрес, перевантаження. Ви можете відчувати, що не розумієте, що відбувається, не можете зосередитися, не знаєте, що робити, яке рішення прийняти, як відповісти співрозмовнику тощо. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "intense_unbearable_emotions": {
        "name": "Сильні, нестерпні емоції",
        "description": "Сильні емоції в цілому складніше витримувати, ніж слабкі чи помірні, але у ситуаціях тривалого чи інтенсивного стресу навіть відносно слабкі емоції можуть відчуватися як нестерпні. Ви можете відчувати, що не витримуєте силу емоції, або що емоції “накривають” Вас, не дають ясно мислити і ефективно діяти. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "weakness_helplessness": {
        "name": "Слабкість, безсилля",
        "description": "Безсилля — це варіант емоційної реакції, що по-різному впливає на поведінку. Переживаючи безсилля, Ви можете як завмирати, так і діяти, як раніше, при цьому відчуваючи, що Ви знесилені, не справляєтесь і не можете вплинути на ситуацію. Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "shock_stupor_freeze": {
        "name": "Шок, завмирання",
        "description": "Шок і завмирання — це захисні реакції, що виникають як відповідь на перевантаження, надміру інтенсивні емоції і відчуття. Вони можуть бути менш або більш вираженими — від відчуття емоційного “заніміння” до нездатності нормально реагувати на зовнішні стимули (звуки, звернення людей тощо), повної неможливості відчувати власне тіло, включаючи біль (дисоціація) і неможливості діяти (ступор). Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      },
      "emotional_thoughts": {
        "name": "Емоційні думки",
        "description": "Мислення у стресі досить легко змінюється під впливом сильних емоцій, особливо у ситуації неможливості вплинути на джерело дискомфорту. Думки стають більш категоричними, вони описують лише одну сторону реальності або стають відверто ірраціональними. “Я в усьому винен”, “Все пропало” тощо — типові приклади таких думок. Окремий випадок — думки про самогубство, спричинені сильними емоціями, коли метою є не припинення життя, а полегшення нестерпного страждання. Якщо подолати думки про самогубство не виходить — будь ласка, зверніться за допомогою до спеціаліста (натисніть кнопку “зв’язатися зі спеціалістом”, що з’явиться після всіх інструкцій). Натисніть кнопку “почати”, і Ви отримаєте ряд простих інструкцій, що допоможуть Вам покращити Ваше самопочуття. Переходьте до наступної інструкції лише після того, як виконаєте попередню."
      }
    }
  },
  "quick_help_exercise": {
    "breathing_1": {
      "name": "Дихання",
      "description": "Зробіть від п’яти до двадцяти повних зітхань, схожих на природне зітхання полегшення: повний вдих усім тілом — так, щоб у дихання включилися живіт, діафрагма (коли Ви дихаєте ніби спиною), ребра і грудна клітина — і розслаблений, шумний видих. Робіть такі зітхання підряд, без пауз, рахуючи подумки або загинаючи пальці. Найкраще дихати ротом — так буде легше розслабитися. Дихайте у власному темпі, не поспішаючи, так, щоб Ви встигали розслаблятися на кожному видиху. Якщо у Вас не виходить одразу зітхнути вільно — не страшно, з часом напруження зменшиться, сформується навичка і Ви зможете зітхнути повністю вільно. Можливо, Ви відчуєте запаморочення чи інші ефекти гіпервентиляції (заніміння у м’язах, особливо обличчя, рук, поколювання тощо). Якщо це неприємно — дихайте повільніше. Якщо це не викликає неприємних відчуттів — продовжуйте. Можна зробити і більше, ніж двадцять зітхань — до появи відчуття, що стан змінився, Вам стало легше і приємніше."
    },
    "physical_exercise_1": {
      "name": "Увага до відчуттів, заземлення, розслаблення",
      "description": "Зверніть увагу на тілесні відчуття. Помітьте, де у тілі є напруга, де Ви відчуваєте будь-які відчуття, що з цього приємно, а що — ні. Керовану увагу можна поєднати з вільним диханням (повний вдих — розслаблений видих, не поспішаючи, без пауз). Помітьте, наскільки розслаблена Ваша шия, якщо відчуваєте напругу — розслабте шию: найпростіший спосіб — похитати головою у різні сторони, обережно та без поспіху, протягом хвилини-двох. Якщо у тілі є напруга, спрямуйте увагу у напружене місце, зафіксуйте увагу на цьому місці і вільно зітхніть три-п’ять разів. Видих природним чином допомагає розслабити м’язи. Коли Ви розслабитеся більше — спрямуйте увагу на відчуття від ніг. Якщо Ви погано відчуваєте ноги — поворушіть пальцями ніг, спробуйте зловити ними підлогу (навіть якщо Ви у взутті). Відчуйте опору під ногами, помітьте відчуття контакту з опорою. Це також можна поєднати з вільним диханням (п’ять-десять зітхань, не поспішаючи, без пауз)."
    },
    "physical_exercise_2": {
      "name": "Зевание",
      "description": "Зверніть увагу на тілесні відчуття. Помітьте, де у тілі є напруга, а де навпаки — слабкість у м’язах. Потягніться декілька разів так, як Вам хочеться, як буде приємно і комфортно. Поєднайте потягування з вільним диханням (п’ять-десять зітхань, не поспішаючи, без пауз). Робіть це стільки, скільки буде потрібно, щоб відчути, що стан змінився, тіло стало більш розслабленим і бадьорим — або доки Вам буде приємно і цікаво продовжувати."
    },
    "physical_exercise_3": {
      "name": "Позіхання",
      "description": "Кероване позіхання дозволяє швидко зняти надмірне напруження. Для того, щоб позіхнути за бажанням, розслабте щелепу, зробіть максимально повний вдих (не надто стараючись і не поспішаючи, але так, щоб вдих був насправді повним, на весь об’єм легень) — а потім, не видихаючи, вдихніть ще трохи, і ще трохи — доки це буде фізично можливо. Якщо у Вас не виходить позіхнути одразу — не страшно, навіть неуспішні спроби теж допомагають розслабитися. Швидше за все, у якийсь момент у Вас таки вийде позіхнути за власним бажанням. Повторіть це п’ять-сім разів."
    },
    "breathing_2": {
      "name": "Дихання",
      "description": "Резонансне дихання — одна з найпростіших і при цьому ефективних дихальних технік, яка добре працює для подолання наслідків тривалого стресу, особливо за регулярного застосування. На відміну від інших дихальних технік, резонансне дихання найкраще робити сидячи. Протягом п’яти хвилин робіть вдих на п’ять секунд і видих на п’ять секунд, без пауз. Візуальна підказка допоможе Вам рахувати секунди."
    },
    "breathing_3": {
      "name": "Дихання",
      "description": "Чергування прискореного і сповільненого дихання допомагає швидко змінити і емоційний, і тілесний стан за рахунок фізіології дихання, незалежно від думок, що виникають у процесі, а також незалежно від типу стресору. Зробіть п’ять дихальних циклів наступним чином: кожен цикл включає чотири швидких вдихи-видихи (вдих на одну секунду — видих на одну секунду, без пауз) і один повільний (вдих на чотири секунди — видих на чотири секунди, без пауз). Таким чином, всього у Вас вийде двадцять п’ять вдихів-видихів. Ймовірно, Ви відчуєте запаморочення та інші ефекти гіпервентиляції (заніміння і поколювання у м’язах, найчастіше — рук і обличчя). Це нормальні ефекти, що самі пройдуть після завершення вправи. Візуальна підказка допоможе Вам рахувати секунди."
    },
    "breathing_4": {
      "name": "Дихання",
      "description": "Сповільнене дихання дає заспокійливий ефект, але сповільнити дихання одразу часто буває складно у зв’язку з м’язовою напругою. Тому простіше поступово сповільнювати дихання, і при цьому не поспішаючи розслабляти напружені м’язи. Вдихайте на одну секунду — видихайте на дві, вдихайте на три — видихайте на чотири, вдихайте на п’ять — видихайте на шість і т.д. до шістнадцяти секунд. За бажанням, якщо це виходить, можна продовжити вправу (вдих на сімнадцять секунд — видих на вісімнадцять). Візуальна підказка допоможе Вам рахувати секунди."
    },
    "breathing_5": {
      "name": "Дихання",
      "description": "Ця дихальна вправа досить інтенсивна, тому якщо — особливо на початку — ефекти будуть надміру сильними для Вас, Ви можете пропустити цей пункт і повернутися до нього пізніше. Зробіть двадцять швидких вдихів-видихів (вдих на одну секунду — видих на одну секунду, без пауз) — а потім паузу на тридцять секунд. Після паузи вдихніть — і затримайте дихання ще на п’ятнадцять секунд. Після цього зробіть ще двадцять швидких вдихів-видихів (так само, як перед цим) — і зробіть паузу на хвилину. Після паузи вдихніть — і затримайте дихання ще на тридцять секунд. У завершення зробіть ще двадцять швидких вдихів-видихів — а потім паузу на півтори хвилини. Після паузи вдихніть — і затримайте дихання ще на сорок п’ять секунд. Якщо такі паузи занадто довгі і Вам захочеться вдихнути раніше — це можна зробити. Вправа дійсно інтенсивна і може викликати емоційний сплеск. Це теж нормально, але якщо це занадто сильні відчуття — просто пропустіть або відкладіть вправу. Коли будете готові — натисніть на кружечок під інструкцією."
    },
    "containment": {
      "name": "Контейнування",
      "description": "Фізіологія емоцій така, що не дозволяє нам довільно обирати, коли емоційна реакція почнеться і завершиться, проте ми можемо обирати поведінкові реакції на емоцію. На рівні поведінки ми можемо обрати стримування емоцій — взяти себе в руки, не показувати емоції зовні, не давати їм волю всередині. Це дає нам змогу “зберегти лице” і інколи — безпеку (коли емоції лякають нас своєю силою), проте має відтерміновані несприятливі ефекти. Також ми можемо обрати експресію — більш чи менш бурхливе вираження емоцій, що корисніше з точки зору впливу на здоров’я, але менш корисно з точки зору впливу на наш образ у власних очах та в очах оточуючих. Третій варіант — контейнування (або, в інших термінах, проживання, витримування, конфронтація) передбачає збереження контакту з емоцією та проживання її (емоція проходить свій природній цикл) без втрати контролю та небажаних зовнішніх проявів. Отже, контейнування робиться наступним чином:\n\n1. Назвіть емоцію, що охопила Вас. Якщо вона занадто сильна, аби її назвати — оберіть відповідний варіант зі списку (у ньому будуть представлені лише негативні емоції і почуття, адже з позитивними зазвичай не виникає проблем): \n\n\t- печаль (сум, смуток, відчай, горе, пригніченість, туга)\n\t- гнів (злість, роздратування, ненависть, лють)\n\t- страх (тривога, занепокоєння, хвилювання, жах, паніка)\n\t- сором\n\t- провина\n\t- образа\n\t- заздрість\n\t- ревнощі\n\t- безсилля\n\t- розгубленість\n\t- відраза\n\t- біль\n\n2. Назвіть причину, що викликала це почуття. Цей пункт необов’язковий для контейнування, проте найчастіше співвіднесення почуття з його причиною заспокоює та допомагає відчути полегшення. \n\n3. Зверніть увагу на тілесні відчуття та знайдіть у тілі ті реакції, що пов’язані з цією емоцією.\n\n4. Зітхніть вільно кілька разів (стільки, скільки знадобиться, щоб відчути себе краще): повний вдих — розслаблений, шумний видих. Під час дихання зберігайте увагу на тілесних відчуттях, адже саме вони — ті маркери, на які можна орієнтуватися для вибору тривалості вправи."
    },
    "imagery_rehearsal": {
      "name": "Відреагування в уяві",
      "description": "Переживаючи вплив якогось стресору, психіка починає шукати способи позбутися його чи усунути його джерело. Найвідоміші реакції — це боротьба чи втеча, проте окрім імпульсів “бий” і “біжи” ми можемо відчувати й інші — це завжди максимально прості і не завжди осмислені дії. У більшості випадків реально реалізувати такий імпульс неможливо (зазвичай через соціальні обмеження, інколи через фізичні), і його пригнічення лише посилює стрес. Зверніть увагу на тілесні відчуття. Що хоче робити Ваше тіло — бігти, бити, ховатися, плакати, кричати, закритися, вхопити щось чи когось, відштовхнути, кинути щось тощо? Звільніть дихання (зітхніть кілька разів чи дихайте глибоко і без пауз). Уявіть, як Ви робите те, чого хоче тіло. Уявляйте це стільки, скільки знадобиться, щоб тіло розслабилося, а емоції заспокоїлися. \n\nВажливо! Деякі люди відчувають побоювання щодо агресивних дій — вони думають, що якщо уявлятимуть собі, наприклад, бійку чи знищення якихось предметів, то можуть втратити над собою контроль і дійсно вчинити щось руйнівне. Такі побоювання зрозумілі, проте необґрунтовані — ми добре розрізняємо уявну реальність та фізичну і розуміємо, що в уяві можна те, чого не можна у дійсності."
    },
    "attention_management": {
      "name": "Управління увагою",
      "description": "Зверніть увагу на навколишню обстановку. Що Ви бачите перед собою? Назвіть п’ять предметів і те, з чого вони зроблені (наприклад, “це дерев’яний стілець”, “це скляна чашка”, “це картонна коробка” тощо). Потім знайдіть і назвіть три предмети з дерева (чи металу, пластика, тканини — залежно від того, що є довкола). Назвіть три предмети червоного кольору (або зеленого чи синього, якщо червоного немає). Навіть три предмети круглої форми (квадратної, трикутної). Якщо Ви відчуваєте, що хочете продовжити — оберіть ще три параметри (колір, форма, матеріал) — і зробіть вправу ще раз або декілька разів."
    },
    "normalization_and_positive_meaning_assignment": {
      "name": "Нормалізація та визначення позитивного сенсу симптому",
      "description": "Усі неприємні емоції, відчуття та думки, що виникають на фоні стресу — це наслідок намагань психіки адаптуватися до навколишнього світу. Стимули, що впливають на нас, інколи занадто сильні, аби до них можна було адаптуватися без дискомфорту. У такому разі ми буваємо схильні лякатися власних реакцій та відчувати невдоволення тими відчуттями, почуттями і думками, які у нас виникають. Ці так звані вторинні реакції лише посилюють стрес. У таких випадках покращити власний стан допомагає нормалізація психічних реакцій. Спробуйте визначити і назвати ту функцію, яку виконує Ваш “симптом” (тобто те, що Вам не подобається і що Ви не хотіли б відчувати і переживати). Наприклад, неможливість розслабитися, дратівливість і труднощі з засинанням — це підвищена пильність, необхідна для безпеки. Тремтіння чи напад паніки — спосіб знизити напруження. Нічні жахи — інструмент переробки неприємного і болісного досвіду тощо. Подумайте, яких “цілей” хоче досягти Ваша психіка, створюючи неприємну реакцію? Навіть якщо відповідь не прийти одразу, таким чином Ви спрямуєте увагу на більш конструктивний фокус."
    },
    "somatic_knot": {
      "name": "Тілесний вузол",
      "description": "У багатьох випадках заспокоїтися і відчути полегшення допомагає планування дій — особливо якщо ці дії допомагають позбутися стресору, що викликав реакцію. Техніку для такого планування найкраще виконувати письмово, але якщо Ви знаходитеся там, де писати неможливо — просто продумуйте відповіді подумки.\n\n1. Факт. Назвіть факт, що викликав у Вас неприємну реакцію. Факт — це щось, що можна побачити чи почути, а не наші враження з цього приводу. Наприклад, “він/вона/вони сказали/зробили” — і далі пряма мова або опис дій.\n\n2. Інтерпретація. Що Ви подумали у зв’язку з тим, що відбулося? Що це значило для Вас? Який сенс Ви вбачаєте у описаній події?\n\n3. Емоція. Назвіть емоцію, яку викликала подія (швидше за все, її інтерпретація, а не сам факт). Це можуть бути сум, гнів, страх, образа, провина, сором, розгубленість, безсилля, відраза, біль — чи якась комбінація почуттів.\n\n4. Відчуття. Зверніть увагу на відчуття у тілі. Де Ви помічаєте реакцію на цю емоцію — і яка вона? Якщо Ви звільните дихання (зробите декілька зітхань або почнете дихати глибоко, повільно і без пауз) — робити цю практику буде легше і комфортніше.\n\n5. Імпульс. Що хочеться зробити у зв’язку з цією емоцією та її переживанням на рівні відчуттів? Імпульс — це завжди максимально проста дія (бити, бігти, плакати, кричати, ховатися, закритися, відштовхнути, схопити тощо). Якщо бажання сильне — зробіть паузу на 2-3 хвилини, звільніть дихання і уявіть, як Ви робите цю дію. \n\n6. Варіанти дій. Напишіть усі варіанти дій, що можливі у даній ситуації. Не всі з них можуть бути реалістичними, реально можливими чи конструктивними — важливий сам процес створення ідей, а не моментальний пошук рішення.\n\n7. Вибір дії. Оберіть те, що Ви насправді збираєтеся зробити. Це може бути варіант зі списку, комбінація варіантів — або щось нове. Також Ви можете обрати нічого не робити чи відкласти прийняття рішення."
    },
    "visualization": {
      "name": "Візуалізації",
      "description": "Перед початком слід сказати, що якщо у Вас не вийде виконати цю техніку з першого разу — не страшно, це в порядку. Ви можете спробувати виконати її знову через деякий час. Бажано виконувати її у місці, де десять-п’ятнадцять хвилин Вас не будуть турбувати і відволікати. Виконання цієї практики передбачає, що Ви заплющите очі, тому спочатку прочитайте всю інструкцію, а потім починайте. \n\nЗаплющіть очі і зверніть увагу на тілесні відчуття. Якщо Ви помічаєте, що деякі м’язи напружені, спрямуйте увагу до цих місць і зробіть декілька вільних зітхань (повільний глибокий вдих — розслаблений видих). Уявіть собі місце, де спокійно і безпечно. Це може бути приміщення, ландшафт чи навіть окрема планета. Роздивіться, що там знаходиться. Спробуйте дати образу розвиватися своїм ходом, так, щоб елементи з’являлися ніби незалежно від Вас, але якщо нічого не приходить — Ви можете змоделювати образ самостійно. Що Ви бачите, чуєте, відчуваєте у цьому місці? Чи потрібен тут хтось іще, а чи найкраще бути на самоті? Уявіть, як Ви робите те, що хочеться. Підтримуйте цей образ стільки, скільки буде необхідно для полегшення і відновлення сил."
    }
  },
  "long_time_work": {
    "title": "Тривала робота",
    "return_page_title": "Повернутись до вибору дня",
    "description": "Тривала робота підійде Вам, якщо Ви пережили тривалий стрес і потребуєте відновлення. Максимальний результат Ви отримаєте, якщо виконуватимете усі завдання, проте якщо це не виходить — робіть так, як виходить, а якщо знадобиться — Ви можете пройти увесь цикл ще раз. Для виконання завдань найкраще завести окремий блокнот чи зошит (краще, якщо Ви писатимете на папері, але якщо це неможливо, то записи у ґаджеті теж підійдуть). Почніть з першого дня і продовжуйте виконувати завдання, додаючи до них нові кожні кілька днів.",
    "1_day": {
      "name": "Перший \nдень",
      "button": "Перший день",
      "instruction": "Почніть з самооцінки Вашого стану. Випишіть у стовпчик усе, що Вас турбує. Це можуть бути неприємні тілесні відчуття, негативні емоції (або, наприклад, емоційна нестабільність, часті зміни настрою), неприємні думки чи небажана поведінка. Увечері кожного дня оцінюйте силу, з якою проявлявся той чи інший показник. Найпростіше оцінювати по шкалі від 0 до 2, де 0 значить, що сьогодні Ви цього не відчували, 1 — що відчували і 2 — що це було дуже сильним. Такий аналіз допомагає не лише зрозуміти, на які симптоми слід звернути більше уваги, але і побачити, як змінюється Ваш стан за час роботи над його поліпшенням. \n\nРобіть дихальні вправи щодня. Тричі на день протягом п’яти хвилин практикуйте резонансне дихання (вдих на п’ять секунд — видих на п’ять секунд без пауз, але якщо цей час занадто великий і так дихати не виходить або неприємно — робіть вдих і видих на чотири секунди, або на три). Найкраще виконувати цю практику сидячи."
    },
    "5_day": {
      "name": "П’ятий \nдень",
      "button": "П’ятий день",
      "instruction": "Виконуйте вправи на релаксацію і роботу з уявою щодня. Найкраще, якщо Ви виконуватимете їх в один і той самий час, проте це не обов’язково. На виконання цієї вправи Вам знадобиться десять-п’ятнадцять хвилин. Заплющіть очі і зверніть увагу на тілесні відчуття. Помітьте, де Ви відчуваєте напругу — і спрямуйте туди увагу. Зробіть п’ять-шість глибоких повільних зітхань без пауз, це допоможе розслабити м’язи. Якщо розслабитися важко, можна спочатку напружити м’язи на десять-п’ятнадцять секунд, зберігаючи при цьому вільне дихання. Уявіть собі місце, де Вам приємно і безпечно. Добре, якщо образи приходять нібито самі, але якщо нічого не з’являється — Ви можете змоделювати картинку чи перебрати декілька варіантів. Що Ви бачите у цьому образі, що чуєте, відчуваєте? Що хочеться робити? Уявіть, як Ви робите те, що хочеться. \n\nНаступна вправа призначена для моментів, коли Ви відчуваєте, що стан погіршився. Опишіть, що Ви переживаєте: які емоції, відчуття, думки та імпульси? Запишіть мінімум три чинники (можна чотири-п’ять), що призвели до погіршення самопочуття. Це можуть бути події, новини, проблеми зі здоров’ям, втома чи навіть зміна погоди. Під кожним з перелічених чинників запишіть, що Ви можете зробити: якщо проблему можна усунути — що Ви зробите, якщо не можна — як можна компенсувати неприємності чи підтримати себе. Важливо! Якщо Ви відчуваєте, що цього недостатньо, поверніться до розділу “швидка допомога”."
    },
    "10_day": {
      "name": "Десятий \nдень",
      "button": "Десятий день",
      "instruction": "Конкретні цілі змінюють нашу увагу і сприйняття: ми починаємо більше фокусуватися на конструктивній повістці і тому, на що ми можемо вплинути. Напишіть список цілей (оберіть той термін, що Вам підходить — це може бути тиждень, місяць чи більше). Якщо ціль велика — розпишіть конкретні задачі, котрі треба виконати для її досягнення. Якщо треба — додайте список бар’єрів, з якими Ви можете стикнутися, а також стратегії на цей випадок. Також можна додати список внутрішніх і зовнішніх ресурсів, до яких можна звернутися для досягнення цієї цілі (тоді у Вас вийде список з трьох частин: ціль, бар’єри, ресурси).\n\nРежим — важлива складова турботи про себе і своє ментальне здоров’я. Щоденно обирайте три приємних дії, які Ви можете зробити. Навіть якщо це буде щось незначне — це не головне, важливо, щоб це приносило задоволення (чи хоча б налаштовувало на нього). Це може бути спілкування, спорт, їжа, музика, прогулянки тощо. Кожного дня пробуйте додавати щось нове, але якщо дії повторюватимуться — так теж добре."
    },
    "15_day": {
      "name": "П’ятнадцятий \nдень",
      "button": "П’ятнадцятий день",
      "instruction": "Проаналізуйте, наскільки збалансовані Ваші щоденні навантаження. Всі навантаження можна умовно поділити на фізичні, емоційні (спілкування, мистецтво, новини тощо) та інтелектуальні (робота, навчання тощо). Якщо є сфера, що явно займає менше місця і часу, сплануйте, як Ви можете вирівняти баланс. Ви можете прибрати щось з “перевантаженої” сфери (якщо це можливо) або додати щось у сферу, якої недостатньо. \n\nЗверніть увагу на те, який позитивний досвід Ви можете привнести у своє життя. Бажано, щоб це було щось приємне (нова їжа, нові місця і маршрути, нова музика тощо), але іноді це може бути і щось нейтральне. Новий досвід необхідний нам з багатьох причин, але якщо це завдання важко виконати одразу — почніть з того, щоб звертати увагу на те, як і де отримувати новий приємний досвід було б можливо. З цієї сходинки перейти до практики буде простіше."
    }
  },
  "start_exercise_widget": {
    "start": "старт",
    "pause": "пауза",
    "inhale": "вдих",
    "exhale": "видих"
  }
};
static const Map<String,dynamic> ru = {
  "common_buttons": {
    "start": "Начать",
    "next": "Дальше",
    "start_over": "Начать сначала",
    "it_helped": "Это помогло",
    "contact_specialist": "Связаться со специалистом",
    "alarm": "Будильник",
    "ok": "ОК"
  },
  "language_selection_screen": {
    "return_page_title": "Вернуться к выбору языка",
    "title": "ВЫБОР ЯЗЫКА"
  },
  "therapy_options_screen": {
    "quick_help": "Быстрая помощь",
    "long_term_work": "Длительная работа",
    "prevention": "Профилактика",
    "return_page_title": "Вернуться к типу помощи",
    "title": "ТИП ПОМОЩИ"
  },
  "about_app_screen": {
    "title": "О приложении",
    "description": "Это свободная программа: вы можете перераспространять её и/или изменять её на условиях Стандартной общественной лицензии ГНУ в том виде, в каком она была опубликована Фондом свободного программного обеспечения; либо 3-й версии лицензии, либо (по вашему выбору) любой более поздней версии.\n\nЭта программа распространяется в надежде, что она будет полезной, но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной общественной лицензии ГНУ.\n\nВы должны были получить копию Стандартной общественной лицензии ГНУ вместе с этой программой. Если это не так, см. <https://www.gnu.org/licenses/>."
  },
  "contacts_screen": {
    "title": "О нас и связи с нами",
    "description": "Обратиться за профессиональной помощью — лучшее решение во многих ситуациях. Здесь Вы найдёте контакты специалистов, которые создали это приложение.",
    "language_title": "ЯЗЫКИ ",
    "contacts_title": "КОНТАКТЫ ",
    "yana": {
      "name": "Яна",
      "surname": "Штагер",
      "languages": "Украинский, Русский, Английский",
      "description": "Клинический психолог, телесно-ориентированный психотерапевт",
      "email": "shtagerpsiholog@gmail.com",
      "youtube": "channel/UC5BVpYpa650pr5B1iTdqkEQ"
    },
    "andrey": {
      "name": "Андрей",
      "surname": "Флешель",
      "languages": "Украинский, Русский, Английский",
      "description": "Психотерапевт, коуч, гипнотерапевт, супервизор",
      "email": "fleshelll@gmail.com",
      "youtube": "@AndreiFleshel/featured",
      "site": "fleshel.info"
    },
    "email_message": {
      "subject": "Личное сообщение UAWell",
      "template": "Здравствуйте! Я хочу узнать больше о возможности индивидуальной консультации"
    },
    "email": "email",
    "youtube": "youtube",
    "site": "site"
  },
  "prevention_screen": {
    "return_page_title": "Вернуться к параметрам",
    "title": "Профилактика",
    "description": "Профилактика предполагает, что Вы чувствуете себя хорошо, она предназначена для предотвращения неприятных состояний, для того, чтобы чувствовать себя бодрее и свободнее. Если Вы заметите, что Вам необходимы практики для работы с дискомфортом или проблемой, Вы можете вернуться к разделу «быстрая помощь». Здесь Вы можете выбрать интенсивность работы — от минимальной до максимальной, в зависимости от Ваших потребностей на данный момент. Выберите интенсивность и длительность программы — и переходите к описанию заданий.",
    "intensity": {
      "title": "Выберите интенсивность",
      "minimum": {
        "name": "Минимальная",
        "description": "Резонансное дыхание — одна из самых простых, но эффективных дыхательных техник, которая хорошо работает не только для устранения последствий длительного стресса, но и для поддержания хорошего самочувствия, особенно при регулярном применении. В отличие от остальных дыхательных техник, резонансное дыхание лучше делать сидя. На протяжении пяти минут делайте вдох на пять секунд и выдох на пять секунд, без пауз. Визуальная подсказка поможет Вам отсчитывать секунды."
      },
      "middle": {
        "name": "Средняя",
        "description": "Начните с того, чтобы делать дыхательные упражнения ежедневно. Резонансное дыхание — одна из самых простых, но эффективных дыхательных техник, которая хорошо работает не только для устранения последствий длительного стресса, но и для поддержания хорошего самочувствия, особенно при регулярном применении. В отличие от остальных дыхательных техник, резонансное дыхание лучше делать сидя. На протяжении пяти минут делайте вдох на пять секунд и выдох на пять секунд, без пауз. Визуальная подсказка поможет Вам отсчитывать секунды. \n\nДобавьте упражнение по расслаблению и работе с воображением. Лучше всего, если Вы будете делать его в одно время, но это не обязательно. На выполнение этого упражнения Вам потребуется десять-пятнадцать минут. Закройте глаза и обратите внимание на телесные ощущения. Заметьте, где Вы ощущаете напряжение и направьте внимание туда. Сделайте пять-шесть глубоких медленных вздохов без пауз, это поможет расслабить мышцы. Если расслабиться трудно, можно сначала напрячь мышцы на десять-пятнадцать секунд, сохраняя при этом свободное дыхание. Затем представьте место, где Вам приятно и безопасно. Хорошо, если образы появляются как будто сами, но если ничего не приходит — Вы можете смоделировать картинку или перебрать несколько вариантов. Что Вы видите в этом образе, что слышите, ощущаете? Что хочется делать? Представьте, как Вы делаете то, что Вам хочется."
      },
      "maximum": {
        "name": "Максимальная",
        "description": "Начните с того, чтобы делать дыхательные упражнения ежедневно. Резонансное дыхание — одна из самых простых, но эффективных дыхательных техник, которая хорошо работает не только для устранения последствий длительного стресса, но и для поддержания хорошего самочувствия, особенно при регулярном применении. В отличие от остальных дыхательных техник, резонансное дыхание лучше делать сидя. На протяжении пяти минут делайте вдох на пять секунд и выдох на пять секунд, без пауз. Визуальная подсказка поможет Вам отсчитывать секунды. \n\nДобавьте упражнение по расслаблению и работе с воображением. Лучше всего, если Вы будете делать его в одно время, но это не обязательно. На выполнение этого упражнения Вам потребуется десять-пятнадцать минут. Закройте глаза и обратите внимание на телесные ощущения. Заметьте, где Вы ощущаете напряжение и направьте внимание туда. Сделайте пять-шесть глубоких медленных вздохов без пауз, это поможет расслабить мышцы. Если расслабиться трудно, можно сначала напрячь мышцы на десять-пятнадцать секунд, сохраняя при этом свободное дыхание. Затем представьте место, где Вам приятно и безопасно. Хорошо, если образы появляются как будто сами, но если ничего не приходит — Вы можете смоделировать картинку или перебрать несколько вариантов. Что Вы видите в этом образе, что слышите, ощущаете? Что хочется делать? Представьте, как Вы делаете то, что Вам хочется.\n\nКонкретные цели изменяют наше внимание и восприятие: мы начинаем больше фокусироваться на конструктивной повестке и том, на что можем повлиять. Напишите список целей (выберите тот срок, который Вам подходит — это может быть неделя, месяц или больше). Если цель большая — распишите конкретные задачи, которые нужно выполнить для её достижения. Если нужно — добавьте описание барьеров, с которыми Вы можете столкнуться и стратегии на этот случай. Также можно добавить список внешних и внутренних ресурсов, к которым можно обратиться для достижения этой цели (тогда у Вас получится список из трёх частей: цель, барьеры, ресурсы)."
      }
    },
    "duration": {
      "title": "Выберите длительность",
      "two_weeks": "Две недели",
      "three_weeks": "Три недели",
      "four_weeks": "Четыре недели"
    }
  },
  "quick_help": {
    "return_page_title": "Вернуться к симптомам",
    "symptoms": {
      "apathy_empty": {
        "name": "Апатия, опустошённость",
        "description": "Апатия предполагает отсутствие желания и сил что-либо делать, недостаток мотивации, эмоциональную опустошённость и как следствие — уменьшение продуктивности до полной невозможности действовать. Вы можете чувствовать, что ничего не хотите или не можете, потому что нет сил или смысла. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "anxiety_agitation": {
        "name": "Беспокойство, тревожная суета",
        "description": "Тревога — это вариант нормальной реакции на реальную или предполагаемую угрозу. Тревога часто сопровождается мобилизацией (активацией психики и тела, чтобы избежать угрозы), которая не всегда приводит к продуктивным действиям. Особенно в тех ситуациях, где сделать что-то полезное невозможно (в ситуациях бессилия). Вы можете чувствовать, как из-за беспокойства не можете усидеть на месте, ясно мыслить и эффективно действовать. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "derealization_depersonalization": {
        "name": "Дереализация, деперсонализация",
        "description": "Дереализация — это изменение восприятия, при котором окружающий мир кажется не таким, как прежде, ненастоящим, далёким, больше похожим на сон или двухмерное изображение. Деперсонализация предполагает ощущение изменения собственной личности. Вы можете чувствовать, что изменились, стали не такими, как раньше, что это не Вы, что не получается чувствовать, ощущать и реагировать так же, как прежде. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "difficulty_breathing": {
        "name": "Затруднённое дыхание, неполный вдох",
        "description": "Стресс, особенно сильный или длительный, ощутимо влияет на рисунок дыхания. Вы можете чувствовать, что дыхание стало поверхностным, недостаточным, не получается сделать полный вдох или полный выдох, что Вам не хватает воздуха. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "tension_relaxation_impossibility": {
        "name": "Напряжение, невозможность расслабиться",
        "description": "Стресс, особенно сильный или длительный, часто ощутимо влияет на мышечный тонус. Вы можете чувствовать, что чрезмерно напряжены и не можете расслабиться, или что возможно расслабиться, но затем напряжение быстро возвращается. Иногда это приводит к появлению боли в мышцах или уменьшению подвижности. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "unpleasant_physical_reactions": {
        "name": "Неприятные телесные реакции",
        "description": "Стресс может вызывать различные неприятные телесные реакции, например, тошноту, онемение, озноб, жар, головокружение и т. д. Эти реакции могут быть более или менее сильными и длительными. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "panic": {
        "name": "Паника",
        "description": "Паника — это приступ сильной тревоги, который обычно длится от нескольких до нескольких десятков минут и предполагает интенсивные телесные реакции — головокружение, напряжение мышц или слабость, изменение ощущения температуры тела, сердцебиение и пр. Также возможны характерные мысли — о том, что это состояние опасно, оно предвещает обморок, сердечный приступ, нездоровое поведение, безумие или смерть. Панические приступы безопасны для психического и физического здоровья, хотя и очень неприятны. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "immersion_painful_experience_flashback_traumatic_funnel": {
        "name": "Погружение в болезненный опыт («флешбек», «травматическая воронка»)",
        "description": "Погружение в болезненный опыт чаще всего оказывается реакцией на триггер (что-то, что напоминает травматические обстоятельства — звук, событие, запах и пр.), но может происходить и само по себе. В этот момент психика реагирует так, как будто опасность из прошлого вернулась. Такие реакции сопровождаются сильными эмоциями и изменением поведения (замирание или действия, которые соответствуют воспринимаемой опасности). Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "bewilderment_inability_concentrate": {
        "name": "Растерянность, невозможность сосредоточиться",
        "description": "Растерянность может быть более или менее интенсивной реакцией, которая возникает как ответ на стресс, перегрузку. Вы можете чувствовать, что не понимаете, что происходит, не можете сосредоточиться, не знаете, что делать, какое решение принять, как ответить собеседнику и пр. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "intense_unbearable_emotions": {
        "name": "Сильные/непереносимые эмоции",
        "description": "Сильные эмоции в целом переносятся сложнее слабых или умеренных, но в ситуации длительного или интенсивного стресса даже относительно слабые эмоции могут чувствоваться как непереносимые. Вы можете чувствовать, что не выдерживаете эмоционального накала, что эмоции накрывают Вас, не дают ясно думать и эффективно действовать. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "weakness_helplessness": {
        "name": "Слабость, бессилие",
        "description": "Бессилие — это вариант эмоциональной реакции, которая по разному влияет на поведение. Переживая бессилие, Вы можете как бездействовать, так и действовать как прежде, при этом чувствуя, что Вы обессилены, не справляетесь и не можете повлиять на ситуацию. Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "shock_stupor_freeze": {
        "name": "Шок, ступор, замирание",
        "description": "Шок и замирание — защитные реакции, которые возникают как ответ на перегрузку, чрезмерно интенсивные эмоции и ощущения. Они могут быть более или менее выраженными, от ощущения эмоционального «онемения» до неспособности нормально реагировать на внешние стимулы (звуки, обращения людей и пр.), полной невозможности чувствовать своё тело, включая боль (диссоциация) и неспособности действовать (ступор). Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      },
      "emotional_thoughts": {
        "name": "Эмоциональные мысли",
        "description": "Мышление в стрессе довольно легко изменяется под воздействием сильных эмоций, особенно в ситуации невозможности повлиять на источник дискомфорта. Мысли становятся более категоричными, отражающими только одну сторону реальности или откровенно иррациональными. «Я во всём виноват», «Всё пропало» и т. д. - типичные примеры таких идей. Частный случай — мысли о самоубийстве, вызванные сильными эмоциями, когда целью является не прекращение жизни, а облегчение страдания. Если Вы не можете справиться с мыслями о смерти, пожалуйста, обратитесь за помощью к специалисту (нажмите на кнопку «связаться со специалистом», которая появится после всех инструкций). Нажмите на кнопку «начать», и Вы получите ряд простых инструкций, которые помогут Вам улучшить Ваше самочувствие. Переходите к следующей инструкции только после того, как выполните предыдущую."
      }
    }
  },
  "quick_help_exercise": {
    "breathing_1": {
      "name": "Дыхание",
      "description": "Сделайте от пяти до двадцати полных вздохов, похожих на естественный вздох облегчения: полный вдох всем телом — так, чтобы в дыхание включались живот, диафрагма (когда Вы дышите словно бы спиной), рёбра и грудная клетка — и расслабленный шумный выдох. Делайте такие вздохи подряд, без пауз, считая их в уме или загибая пальцы. Лучше всего дышать ртом, так расслабиться будет легче. Дышите в своём темпе, не спеша, так, чтобы Вы успевали расслабляться на каждом выдохе. Если у Вас не получается сразу сделать свободный вздох — ничего страшного, со временем напряжение уменьшится, сформируется навык и Вы вздохнёте свободно. Возможно, Вы почувствуете головокружение или другие эффекты гипервентиляции (онемение в мышцах, особенно лица и рук, покалывание и пр.). Если это неприятно — замедлитесь, если же это не вызывает неприятных ощущений — продолжайте. Можно сделать и больше двадцати вздохов — до ощущения, что состояние поменялось, Вам стало легче и комфортнее."
    },
    "physical_exercise_1": {
      "name": "Внимание к ощущениям, заземление, расслабление",
      "description": "Обратите внимание на телесные ощущения. Заметьте, где в теле есть напряжение, где Вы чувствуете какие-либо ощущения, что из этого приятно, а что — нет. Направленное внимание можно сочетать со свободным дыханием (полный вдох — расслабленный выдох не спеша, но без пауз). Заметьте, насколько расслаблена Ваша шея, и если Вы чувствуете напряжение — расслабьте шею: самый простой способ — «поболтать» головой в разные стороны, аккуратно и неспешно, на протяжении минуты-двух. Если в теле есть напряжение, направьте внимание в напряжённое место, зафиксируйте его и вздохните свободно три-пять раз. Выдох естественным образом помогает расслабить мышцы. Когда Вы расслабитесь больше — направьте внимание на ощущения от ног. Если Вы плохо чувствуете ноги — пошевелите пальцами ног, попробуйте поймать ими пол (даже если Вы в обуви). Почувствуйте опору под ногами, заметьте ощущения от контакта с опорой. Это также можно соединить со свободным дыханием (пять-десять вздохов, не спеша, без пауз)."
    },
    "physical_exercise_2": {
      "name": "Потягивание",
      "description": "Обратите внимание на телесные ощущения. Заметьте, где в теле есть напряжение, а где наоборот — слабость мышц. Потянитесь несколько раз так, как Вам хочется, как будет приятно и комфортно. Соединяйте потягивание со свободным дыханием (пять-десять вздохов не спеша, без пауз). Делайте это столько, сколько потребуется, чтобы ощутить, что состояние изменилось, тело ожило и расслабилось — или пока Вам будет приятно и интересно продолжать."
    },
    "physical_exercise_3": {
      "name": "Зевание",
      "description": "Намеренное зевание позволяет быстро снять чрезмерное напряжение. Для того, чтобы зевать по желанию, расслабьте челюсть, сделайте максимально полный вдох (не слишком стараясь и не спеша, но так, чтобы вдох был действительно полным, на весь объём лёгких) — а затем, не выдыхая, вдохните ещё немного, и ещё немного, пока это будет получаться. Если у Вас не выходит осуществить это сразу — ничего страшного, попытки также оказывают расслабляющий эффект. Скорее всего, в какой-то момент у Вас получится произвольно зевнуть. Повторите это пять-семь раз."
    },
    "breathing_2": {
      "name": "Дыхание",
      "description": "Резонансное дыхание — одна из самых простых, но эффективных дыхательных техник, которая хорошо работает для устранения последствий длительного стресса, особенно при регулярном применении. В отличие от остальных дыхательных техник, резонансное дыхание лучше делать сидя. На протяжении пяти минут делайте вдох на пять секунд и выдох на пять секунд, без пауз. Визуальная подсказка поможет Вам отсчитывать секунды."
    },
    "breathing_3": {
      "name": "Дыхание",
      "description": "Чередование ускоренного и замедленного дыхания помогает быстро изменить и эмоциональное, и телесное состояние за счёт физиологии дыхания, независимо от мыслей, которые возникают в процессе, а также независимо от типа стрессора. Сделайте пять дыхательных циклов таким образом: каждый цикл включает четыре быстрых вдоха-выдоха (вдох на одну секунду — выдох на одну секунду без пауз), и один медленный (вдох на четыре секунды — выдох на четыре секунды без пауз). Таким образом, у Вас получится двадцать пять вдохов-выдохов. Скорее всего, Вы ощутите головокружение и другие эффекты гипервентиляции (онемение и покалывание в мышцах, чаще всего — рук и лица). Это нормальные эффекты, которые сами пройдут после завершения упражнения. Визуальная подсказка поможет Вам отсчитывать секунды."
    },
    "breathing_4": {
      "name": "Дыхание",
      "description": "Замедленное дыхание оказывает успокаивающий эффект, но замедлить дыхание сразу часто бывает сложно в связи с мышечным напряжением. Поэтому проще постепенно замедлить дыхание, и при этом не спеша расслабить напряжённые мышцы. Вдыхайте на одну секунду — выдыхайте на две, затем вдыхайте на три — выдыхайте на четыре, вдыхайте на пять — выдыхайте на шесть и т. д. до шестнадцати секунд. При желании, если получается, можно продлить упражнение (вдох на семнадцать секунд — выдох на восемнадцать). Визуальная подсказка поможет Вам отсчитывать секунды."
    },
    "breathing_5": {
      "name": "Дыхание",
      "description": "Это дыхательное упражнение достаточно интенсивное, поэтому если — особенно поначалу — эффекты будут слишком сильными для Вас, Вы можете пропустить этот пункт и вернуться к нему позже. Сделайте двадцать быстрых вдохов-выдохов (вдох на одну секунду — выдох на одну секунду без пауз) — затем паузу на тридцать секунд. После паузы вдохните — и задержите дыхание ещё на пятнадцать секунд. После этого сделайте ещё двадцать быстрых вдохов-выдохов (также, как перед этим) — затем паузу на минуту. После паузы вдохните — и задержите дыхание ещё на тридцать секунд. В завершение сделайте ещё двадцать быстрых вдохов-выдохов — затем паузу на полторы минуты. После паузы вдохните — и задержите дыхание ещё на сорок пять секунд. Если паузы для Вас будут слишком длинными и Вам захочется вдохнуть раньше — это можно сделать. Упражнение действительно интенсивное и может вызвать эмоциональный всплеск. Это также нормально, но если это слишком интенсивно — лучше пропустить его или отложить. Когда будете готовы начать — нажмите на кружочек под инструкцией."
    },
    "containment": {
      "name": "Контейнирование",
      "description": "Физиология эмоций такова, что не позволяет нам произвольно выбирать, когда эмоциональная реакция начнётся или завершится, однако мы выбираем поведенческие реакции на эмоции. На уровне поведения мы можем выбрать подавление эмоций — взять себя в руки, не показывать эмоции внешне, не давать им волю внутренне. Это позволяет нам сохранить лицо и иногда — безопасность (когда эмоции пугают нас своей силой), но имеет отсроченные неблагоприятные эффекты. Также мы можем выбрать экспрессию — более или менее бурное выражение эмоций, которое более рационально с точки зрения влияния на здоровье, но менее — с точки зрения влияния на наш образ в собственных глазах и в глазах окружающих людей. Третий вариант — контейнирование (или, в других терминах, проживание, выдерживание, конфронтация) предполагает сохранение контакта с эмоцией и проживание её (эмоция проходит свой естественный цикл) без потери контроля и нежелательных внешних проявлений. Итак, контейнирование делается следующим образом:\n\n1. Назовите чувство, которая Вас охватило. Если оно слишком сильное для называния — попробуйте выбрать наиболее подходящий вариант из списка (в нём будут представлены только негативные эмоции и чувства, ведь с позитивными обычно не возникает трудностей):\n\n\t- печаль (грусть, огорчение, отчаяние, горе, подавленность, уныние)\n\t- гнев (злость, раздражение, негодование, возмущение, ненависть, ярость)\n\t- страх (тревога, волнение, беспокойство, ужас, паника)\n\t- стыд\n\t- вина\n\t- обида\n\t- ревность\n\t- зависть \n\t- бессилие\n\t- растерянность\n\t- отвращение\n\t- боль\n\n2. Назовите причину, которая вызвала это чувство. Этот пункт необязателен для контейнирования, но чаще всего связывание чувства с причиной успокаивает и помогает почувствовать облегчение.\n\n3. Обратите внимание на телесные ощущения и найдите в теле реакции, связанные с этим чувством.\n\n4. Вздохните свободно несколько раз (столько, сколько потребуется, чтобы почувствовать себя лучше): полный вдох — расслабленный, шумный выдох. Во время дыхания сохраняйте внимание на телесных ощущениях, именно они — те маркеры, на которые можно ориентироваться, выбирая длительность упражнения."
    },
    "imagery_rehearsal": {
      "name": "Отреагирование в воображении",
      "description": "Испытывая воздействие какого-либо стрессора, психика начинает искать способы избавиться от него или устранить его источник. Самые известные реакции — это борьба или бегство, но кроме импульсов «бей» или «беги» мы можем испытывать и другие — это всегда максимально простые и не всегда осмысленные действия. В большинстве случаев реально реализовать такой импульс невозможно (обычно из-за социальных ограничений, иногда — из-за физических), и его подавление усиливает стресс. Обратите внимание на телесные ощущения. Что хочет делать Ваше тело — бить, бежать, прятаться, плакать, кричать, закрыться, схватить что-то или кого-то, оттолкнуть, бросить что-нибудь и т. д.? Освободите дыхание (вздохните несколько раз или дышите глубоко, медленно и без пауз). Представьте, как Вы делаете то действие, которое хочет тело. Представляйте это столько, сколько потребуется, чтобы тело расслабилось, а эмоции успокоились. \n\nВажно! Многие люди опасаются, что если они будут представлять себе агрессивные и разрушительные действия (например, драку или разбивание каких-то предметов), то потеряют контроль над своим поведением и действительно сделают что-то опасное. Такое беспокойство понятно, но необоснованно — мы различаем воображаемую реальность и материальную и осознаём, что в воображении можно то, чего нельзя в действительности."
    },
    "attention_management": {
      "name": "Управление вниманием",
      "description": "Обратите внимание на окружающую обстановку. Что Вы видите перед собой? Назовите пять предметов и то, из чего они сделаны (например, «это деревянный стул», «это стеклянная» чашка, «это картонная коробка» и т. д.). Затем найдите и назовите три предмета из дерева (или металла, пластика, ткани — в зависимости от того, что есть вокруг). Назовите три предмета красного цвета (или синего/зелёного, если красного нет). Назовите три предмета круглой формы (квадратной, треугольной). Если Вы чувствуете, что хотите продолжить — выберите ещё три параметра (цвет, форма, материал) — и сделайте упражнение ещё раз или несколько раз."
    },
    "normalization_and_positive_meaning_assignment": {
      "name": "Нормализация и определение положительного смысла симптома",
      "description": "Все неприятные эмоции, ощущения и мысли, возникающие на фоне стресса — это следствие попыток психики адаптироваться к окружающему миру. Стимулы, воздействующие на нас, иногда слишком сильны, чтобы к ним можно было адаптироваться без дискомфорта. В таком случае мы бываем склонны пугаться собственных реакций и испытывать недовольство теми чувствами, мыслями и прочими проявлениями, которые у нас возникают. Эти так называемые вторичные реакции только усиливают стресс. В таких случаях почувствовать себя лучше помогает нормализация психических реакций. Попробуйте определить и назвать ту функцию, которую выполняет Ваш «симптом» (т. е. то, что Вам не нравится, и что Вы не хотели бы чувствовать и переживать). Например, невозможность расслабиться, раздражительность или трудности засыпания — это повышенная бдительность, необходимая для безопасности. Дрожь или приступ паники — способ снизить накопившееся напряжение. Ночные кошмары — инструмент переработки неприятного и болезненного опыта и т. д. Подумайте, какие «цели и задачи» хотела решить Ваша психика, продуцируя неприятную Вам реакцию? Даже если ответ не придёт сразу, таким образом Вы переключите внимание на более конструктивный фокус."
    },
    "somatic_knot": {
      "name": "Телесный узел",
      "description": "Во многих случаях успокоиться и почувствовать облегчение помогает планирование действий — особенно если эти действия помогают справиться со стрессором, вызвавшим реакцию. Технику для такого планирования лучше всего выполнять письменно, но если Вы находитесь там, где писать невозможно — просто продумывайте ответы в уме.\n\n1. Факт. Назовите факт, который вызвал у Вас неприятную реакцию. Факт — это что-то, что можно увидеть и услышать, а не наши соображения по этому поводу. Например, «он/она/они сказали/сделали» - и далее прямая цитата или описание действий.\n\n2. Интерпретация. Что Вы подумали в связи с произошедшим? Что это значило для Вас? Какой смысл Вы видите в описанном событии?\n\n3. Эмоция. Назовите эмоцию, которую вызвало событие (скорее всего, интерпретация, а не сам факт). Это могут быть грусть, злость, страх, обида, вина, стыд, растерянность, бессилие, отвращение, боль — или какая-то комбинация чувств.\n\n4. Ощущение. Обратите внимание на ощущения в теле. Где Вы замечаете реакцию на эту эмоцию — и какая она? Если Вы освободите дыхание (сделаете несколько вздохов или начнёте дышать глубоко, медленно и без пауз) — делать эту практику будет легче и комфортнее. \n\n5. Импульс. Что хочется сделать в связи с этой эмоцией и её переживанием на уровне ощущений? Импульс — это всегда максимально простое действие (бить, бежать, плакать, кричать, прятаться, закрыться, отталкивать, схватить и т. д.). Если желание сильное — сделайте паузу на две-три минуты, освободите дыхание и представьте, как Вы делаете это действие.\n\n6. Варианты действия. Напишите все варианты действий, которые возможны в данной ситуации. Не все из них могут быть реалистичными, возможными или конструктивными — важен процесс создания идей, а не мгновенный поиск решения.\n\n7. Выбор действия. Выберите, что Вы действительно собираетесь сделать. Это может быть вариант из списка, комбинация вариантов, что-то новое. Также Вы можете выбрать ничего не делать или отложить принятие решения."
    },
    "visualization": {
      "name": "Визуализации",
      "description": "Перед началом важно сказать, что если у Вас не получится выполнить эту технику с первого раза — ничего страшного, это в порядке. Вы можете попробовать выполнить её снова через некоторое время. Желательно выполнять её в месте, где десять-пятнадцать минут Вас не будут беспокоить и отвлекать. Выполнение этой практики предполагает, что Вы закроете глаза, поэтому сначала прочитайте всю инструкцию, а затем начинайте.\n\nЗакройте глаза и обратите внимания на телесные ощущения. Если Вы замечаете, что некоторые мышцы напряжены, направьте внимание к этим местам и сделайте несколько свободных вздохов (медленный глубокий вдох — расслабленный выдох). Представьте себе место, где спокойно и безопасно. Это может быть помещение, ландшафт или даже отдельная планета. Рассмотрите, что там находится. Попробуйте дать этому образу развиваться своим ходом, так, чтобы элементы появлялись как бы независимо от Вас, но если ничего не приходит — Вы можете смоделировать образ сами. Что Вы видите, слышите, ощущаете в этом месте? Что хочется делать? Нужен ли кто-то ещё, или лучше всего быть там одному? Представьте, что Вы делаете то, что хочется. Поддерживайте этот образ столько, сколько потребуется для облегчения и восстановления сил."
    }
  },
  "long_time_work": {
    "title": "Длительная работа",
    "return_page_title": "Вернуться к выбору дня",
    "description": "Длительная работа подойдёт Вам, если Вы пережили длительный стресс и нуждаетесь в восстановлении. Максимальный результат Вы получите, если будете делать все задания, но если это не получается — делайте так, как получается, а если нужно, Вы можете пройти весь цикл ещё раз. Для выполнения заданий лучше всего завести отдельный блокнот или тетрадь (лучше, если Вы будете писать на бумаге, но если это невозможно, то записи в гаджете тоже подойдут). Начинайте с первого дня и продолжайте выполнять задания, присоединяя к ним новые каждые несколько дней. ",
    "1_day": {
      "name": "Первый \nдень",
      "button": "Первый день",
      "instruction": "Начните с самооценки Вашего состояния. Выпишите в столбик всё, что Вас беспокоит. Это могут быть неприятные телесные ощущения, негативные эмоции (или, например, эмоциональная неустойчивость, частая смена настроения), неприятные мысли или нежелательное поведение. Вечером каждого дня оценивайте силу, с которой проявлялся тот или иной показатель. Проще всего оценивать это по шкале от 0 до 2, где 0 значит, что сегодня Вы этого не чувствовали, 1 — что чувствовали и 2 — что это было очень сильным. Такой анализ помогает не только понять, на какие симптомы нужно обратить больше внимания, но и увидеть, как меняется Ваше состояние по мере работы над его улучшением.\n\nДелайте дыхательные упражнения ежедневно. Три раза в день в течение пяти минут практикуйте резонансное дыхание (вдох на секунд — выдох на пять секунд без пауз, если это время слишком большое и дышать так дискомфортно или не получается — делайте вдох и выдох на четыре секунды, или на три). Лучше всего делать эту практику сидя."
    },
    "5_day": {
      "name": "Пятый \nдень",
      "button": "Пятый день",
      "instruction": "Делайте упражнение по расслаблению и работе с воображением ежедневно. Лучше всего, если Вы будете делать его в одно время, но это не обязательно. На выполнение этого упражнения Вам потребуется десять-пятнадцать минут. Закройте глаза и обратите внимание на телесные ощущения. Заметьте, где Вы ощущаете напряжение и направьте внимание туда. Сделайте пять-шесть глубоких медленных вздохов без пауз, это поможет расслабить мышцы. Если расслабиться трудно, можно сначала напрячь мышцы на десять-пятнадцать секунд, сохраняя при этом свободное дыхание. Затем представьте место, где Вам приятно и безопасно. Хорошо, если образы появляются как будто сами, но если ничего не приходит — Вы можете смоделировать картинку или перебрать несколько вариантов. Что Вы видите в этом образе, что слышите, ощущаете? Что хочется делать? Представьте, как Вы делаете то, что Вам хочется.\n\nСледующее упражнение предназначено для моментов, когда Вы чувствуете, что Ваше состояние ухудшилось. Опишите, что Вы переживаете: какие эмоции, ощущения, мысли и импульсы? Затем запишите как минимум три фактора (можно четыре-пять), которые привели к ухудшению самочувствия. Это могут быть события, новости, проблемы со здоровьем, усталость или даже перемена погоды. Под каждым из описанных факторов запишите, что Вы можете сделать: если проблему можно устранить — то что Вы предпримете, если нельзя — то как можно компенсировать неприятности или поддержать себя. Важно! Если Вы чувствуете, что этого недостаточно, вернитесь к разделу «быстрая помощь»."
    },
    "10_day": {
      "name": "Десятый \nдень",
      "button": "Десятый день",
      "instruction": "Конкретные цели изменяют наше внимание и восприятие: мы начинаем больше фокусироваться на конструктивной повестке и том, на что можем повлиять. Напишите список целей (выберите тот срок, который Вам подходит — это может быть неделя, месяц или больше). Если цель большая — распишите конкретные задачи, которые нужно выполнить для её достижения. Если нужно — добавьте описание барьеров, с которыми Вы можете столкнуться и стратегии на этот случай. Также можно добавить список внешних и внутренних ресурсов, к которым можно обратиться для достижения этой цели (тогда у Вас получится список из трёх частей: цель, барьеры, ресурсы).\n\nРежим — важная составляющая заботы о себе и ментальном здоровье. Ежедневно выбирайте три приятных действия, которые Вы можете сделать. Даже если это будет что-то незначительное — это не главное, важно, чтобы это доставляло Вам удовольствие (или хотя бы располагало к нему). Это может быть общение, еда, спорт, музыка, прогулки и т. д. Каждый день пробуйте добавить что-то новое, но если что-то будет повторяться — так тоже можно."
    },
    "15_day": {
      "name": "Пятнадцатый \nдень",
      "button": "Пятнадцатый день",
      "instruction": "Проанализируйте, насколько сбалансированы Ваши ежедневные нагрузки. Все нагрузки можно условно разделить на физические, эмоциональные (общение, искусство, новости и пр.) и интеллектуальные (работа, учёба и пр.). Если есть сфера, которая явно занимает меньше места и времени, спланируйте, как Вы можете уравновесить баланс. Вы можете убрать что-то из «перегруженной» сферы (если это возможно) или добавить что-то в сферу, которой недостаточно. \n\nОбратите внимание на то, какой новый положительный опыт Вы можете привнести в свою жизнь. Желательно, чтобы это было что-то приятное (новая еда, новые места или маршруты, новая музыка и т. д.), но иногда это может быть и что-то нейтральное. Новый опыт необходим нам по многим причинам, но если это задание сложно выполнить сразу — начните с того, чтобы обращать внимание на то, где и как получить новый приятный опыт было бы возможно. С этой ступеньки будет легче перейти к практике."
    }
  },
  "start_exercise_widget": {
    "start": "старт",
    "pause": "пауза",
    "inhale": "вдох",
    "exhale": "выдох"
  }
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en, "uk": uk, "ru": ru};
}
