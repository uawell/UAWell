import 'package:flutter/material.dart';

class LogoWidget extends StatelessWidget {
  final TextStyle textStyle;

  const LogoWidget({
    Key? key,
    required this.textStyle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 180,
        height: 180,
        child: Stack(
          children: [
            // Изображение
            Image.asset(
              'assets/images/logo.png',
              width: 180,
              height: 180,
            ),
            // Текст по центру
            Center(
              child: Text(
                'UAWell',
                style: textStyle,
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
