import 'dart:async';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';

import '../generated/locale_keys.g.dart';
import '../styles/styles.dart';

class BreathingAnimationWidget extends StatefulWidget {
  final int duration;
  final int breathFrequency;
  final String symptomKey;
  BreathingAnimationWidget({
    Key? key,
    required this.duration,
    required this.breathFrequency,
    required this.symptomKey,
  }) : super(key: key);

  @override
  _BreathingAnimationWidgetState createState() =>
      _BreathingAnimationWidgetState();
}

class _BreathingAnimationWidgetState extends State<BreathingAnimationWidget> {
  bool _isInhale = false;
  Timer? _timer;
  int _elapsedSeconds = 0;
  bool _isTimerRunning = false;
  String _startButton = LocaleKeys.start_exercise_widget_start.tr();

  int _breathFrequency = 5;
  int _duration = 600;


  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!_isTimerRunning) {
          _startTimer();
        } else {
          _stopTimer();
          setState(() {
            _startButton = LocaleKeys.start_exercise_widget_start.tr();
          });
        }
      },
      child: Stack(
        alignment: Alignment.center,
        children: [
          AnimatedContainer(
            duration: Duration(seconds: _breathFrequency),
            width: _isInhale ? 210 : 180,
            height: _isInhale ? 210 : 180,
            child: Image.asset(
              'assets/images/logo.png',
              fit: BoxFit.cover,
            ),
          ),
          Text(
            _startButton,
            style: BreathingAnimationTextStyles.textStyle,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  void _handleTap() {
    setState(() {
      _isInhale = !_isInhale;
      _startButton = _isInhale
          ? LocaleKeys.start_exercise_widget_inhale.tr()
          : LocaleKeys.start_exercise_widget_exhale.tr();
    });
   AudioPlayer().play(AssetSource('sounds/metronome.mp3'));
  }

  void _startTimer() {

    if (!_isTimerRunning) {
      _handleTap();
      _timer = Timer.periodic(Duration(seconds: _breathFrequency), (timer) {

        //  print(_cycleCount);
        //  print(_currentBreathFrequency);
        if (_elapsedSeconds >= _duration) {
          _stopTimer();
        } else {
          _elapsedSeconds++;
        }


        _handleTap();
      });
      _isTimerRunning = true;
    }
  }

  void _stopTimer() {
    _timer?.cancel();
    _isTimerRunning = false;
    _elapsedSeconds = 0;

  }
  static play(String src, Function onComplete, {double playbackRate = 1.0}) async {
      final player = AudioPlayer();
      await player.setPlaybackRate(playbackRate);
      await player.play(AssetSource(src));
      player.onPlayerComplete.listen((_) {
        onComplete();
      });
  }
  @override
  void dispose() {
    _stopTimer();
    super.dispose();
  }
}

