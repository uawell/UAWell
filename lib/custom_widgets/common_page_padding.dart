import 'package:flutter/material.dart';


class CommonPagePadding extends StatelessWidget {
  final Widget commonPage;
  const CommonPagePadding({Key? key, required this.commonPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          padding: const EdgeInsets.fromLTRB(30,  28, 30, 30),
          child: commonPage,
    );
  }
}
