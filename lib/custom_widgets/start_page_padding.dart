import 'package:flutter/material.dart';


class StartPagePadding extends StatelessWidget {
  final Widget startPage;
  const StartPagePadding({Key? key, required this.startPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          padding: const EdgeInsets.fromLTRB(30,  28, 30, 30),
          child: startPage,
    );
  }
}
