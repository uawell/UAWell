import 'package:flutter/material.dart';

class CommonButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String buttonText;

  const CommonButton({
    Key? key,
    required this.onPressed,
    required this.buttonText
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 60;
    double buttonWidth = screenWidth ;
    double scalingFactor = 0.25;
    double buttonHeight = buttonWidth * scalingFactor;
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.symmetric(horizontal: 28, vertical: 10),
        fixedSize: Size(buttonWidth, buttonHeight),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: Color(0xFF3937AC)),
          borderRadius: BorderRadius.circular(100),
        ),
        elevation: 0,
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(
                buttonText,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontSize: 16,
                  color: const Color(0xff3937ac),
                  fontFamily: 'Inter',
                  fontWeight: FontWeight.w500,
                  height: 1.25,
                ),
              ),
            ),
            Image.asset(
              'assets/images/right_arrow.png',
              height: buttonHeight / 8.5 * 17, // Используйте значение по умолчанию или передавайте его как параметр
            ),
          ],
        ),
      ),
    );
  }
}