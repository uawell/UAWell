import 'package:flutter/material.dart';

import '../styles/styles.dart';

class OpenDropDownMenuButton extends StatelessWidget {
  final String buttonText;

  const OpenDropDownMenuButton({Key? key, required this.buttonText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width - 60;
    double buttonWidth = screenWidth;
    double scalingFactor = 0.2;
    double buttonHeight = buttonWidth * scalingFactor;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 28, vertical: 10),
      width: buttonWidth,
      height: buttonHeight,
      decoration: BoxDecoration(
        color: Color(0xFFFAFAFA),
        border: Border.all(
          color: Color(0xFF3A37AC), // Цвет границы
          width: 1, // Ширина границы
        ),
        borderRadius: BorderRadius.circular(30),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(
                buttonText,
                style: SelectOptionsTextStyles.textStyle,
              ),
            ),
            Image.asset(
              alignment: Alignment.centerRight,
              'assets/images/down_arrow.png',
              height: buttonHeight /
                  8.5 *
                  9, // Используйте значение по умолчанию или передавайте его как параметр
            ),
          ],
        ),
      ),
    );
  }
}
