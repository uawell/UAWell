import 'package:flutter/material.dart';

import '../styles/styles.dart';

class CenterTextCommonButton extends StatelessWidget {
  final VoidCallback onPressed;
  final String buttonText;

  const CenterTextCommonButton(
      {Key? key, required this.onPressed, required this.buttonText})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double buttonWidth = screenWidth - 60;
    double scalingFactor = 0.133;
    double buttonHeight = buttonWidth * scalingFactor;
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        padding: EdgeInsets.symmetric(vertical: 8),
        fixedSize: Size(buttonWidth, buttonHeight),
        backgroundColor: Color(0xFF3937AC),
        foregroundColor: Color(0xFF3A37AC),
        shape: RoundedRectangleBorder(
          side: BorderSide(width: 1, color: Color(0xFF3937AC)),
          borderRadius: BorderRadius.circular(100),
        ),
        elevation: 0,
      ),
      child: Text(
          buttonText,
          style: CenterTextCommonButtonStyles.textStyle,
        ),
    );
  }
}
