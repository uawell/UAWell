import 'dart:core';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

class NotificationHandler {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  FlutterLocalNotificationsPlugin();

  NotificationHandler() {
    _initializeNotifications();
  }

  Future<void> _initializeNotifications() async {
    const AndroidInitializationSettings initializationSettingsAndroid =
    AndroidInitializationSettings('app_icon');
    final InitializationSettings initializationSettings =
    const InitializationSettings(android: initializationSettingsAndroid);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  }

  Future<void> _requestNotificationPermission() async {
    flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<
        AndroidFlutterLocalNotificationsPlugin>()?.requestNotificationsPermission();
  }


  Future<void> cancelNotificationsForChannel(String channelId) async {
    await flutterLocalNotificationsPlugin.cancel(0, // 0 - это уникальный ID, но не важно, так как вы отменяете все уведомления этого канала
        tag: channelId);
  }

  Future<void> showScheduledNotifications(String channelId, String title, String body, int hours, int minutes, days) async {
    tz.initializeTimeZones();
    Duration offsetTime = DateTime.now().timeZoneOffset;

    await _requestNotificationPermission();

    final DateTime now = DateTime.now();
    for (int i = 0; i < days; i++) {
      tz.TZDateTime scheduledDateTime =
      tz.TZDateTime(tz.local, now.year, now.month, now.day + i, hours, minutes).subtract(offsetTime);

      // Если время уже прошло, сдвигаем на следующий день
      if (scheduledDateTime.isBefore(tz.TZDateTime.now(tz.local))) {
        scheduledDateTime = scheduledDateTime.add(Duration(days: 1));
        days++;
      }
      print(scheduledDateTime);
      final AndroidNotificationDetails androidPlatformChannelSpecifics =
      AndroidNotificationDetails(
        channelId,
        'Важные уведомления',
        channelDescription: 'ddddd',
        importance: Importance.max,
        priority: Priority.high,
        icon: 'app_icon',
        color: Colors.white,
      );


      final NotificationDetails platformChannelSpecifics =
      NotificationDetails(android: androidPlatformChannelSpecifics);

      try {
        await flutterLocalNotificationsPlugin.zonedSchedule(
          i, // Используйте уникальный ID для каждого уведомления
          title,
          body,
          scheduledDateTime,
          platformChannelSpecifics,
          androidAllowWhileIdle: true,
          uiLocalNotificationDateInterpretation:
          UILocalNotificationDateInterpretation.wallClockTime,
        );
        print('Уведомление успешно запланировано на $scheduledDateTime \n '
            'текущее время ${DateTime.now()}');
      } catch (e) {
        print('Ошибка при планировании уведомления: $e');
      }
    }
  }
}
