import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

import '../generated/locale_keys.g.dart';
import '../sections/menu/menu_screen.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String titleBar;

  const CustomAppBar({
    Key? key,
    required this.titleBar,
  }) : super(key: key);

  @override
  Size get preferredSize => Size.fromHeight(60.0);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0xFFFAFAFA), //or set color with: Color(0xFF0000FF)
      statusBarIconBrightness: Brightness.dark,

    ));
    return PreferredSize(
      preferredSize: preferredSize,
      child: Container(
        margin: EdgeInsets.only(top: 30),
        child: AppBar(
          automaticallyImplyLeading: false,
          toolbarHeight: 20,
          backgroundColor: Colors.transparent,
          surfaceTintColor: Colors.transparent,
          elevation: 0,
          leading: null,
          shadowColor: Colors.transparent,
          flexibleSpace:
              Container(
                child: Row(

                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(width: 30),
                    if (titleBar.isNotEmpty)
                      IconButton(
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.all(0),
                        icon: Image.asset(
                          'assets/images/left_right.png',
                          width: 24,
                          height: 24,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),

                    // Промежуток между иконкой и текстом
                    SizedBox(width: titleBar.isNotEmpty ? 8 : 0),
                    // Текст
                    Text(
                      titleBar.tr(),
                      style: GoogleFonts.getFont(
                        'Inter',
                        color: Color(0xFF9E9CE4),
                        fontSize: 14.0,
                        fontWeight: FontWeight.w400,
                        letterSpacing: 0,
                        height: 1,
                      ),
                    ),

                    // Spacer для заполнения доступного пространства
                    Spacer(),

                    // Иконка справа
                    IconButton(
                      alignment: Alignment.centerRight,
                      icon: Image.asset(
                        'assets/images/burger_button.png',
                        width: 24,
                        height: 24,
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => MenuScreen(),
                          ),
                        );
                      },
                    ),
                    SizedBox(width: 30)
                  ],
                ),
              ),
        ),
      ),
    );
  }
}
